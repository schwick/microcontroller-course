from machine import I2C, Pin
import framebuf
from time import sleep
from SH1107_OLED import OLED

OLED_ADR = const(0x3C)   # decimal: 60

scl = Pin( 32 )
sda = Pin( 33 )

i2c = I2C( 0,scl=scl,sda=sda,freq=100000)

# In this test program we check that we can find the device. This
# allows us to verify that we cabled up everything correctly.
i2cDevices = i2c.scan()

print( "List of I2C devices found during scan:" )

for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()

######### Code to test the display

oled = OLED(i2c, OLED_ADR)
oled.init()

# Micropython has a Framebuffer class which does nice things
# and makes it simple to display text and simple graphics.
# Look at the documentation of this class !
# The memory allocation for the buffer has to be done by us
# and this memory is then passed into the constructor of the
# Framebuffer.
buf = bytearray( 128*16 )

fb = framebuf.FrameBuffer( buf, 128, 128, framebuf.MONO_VLSB )


# Erase the buffer (all black => all bits '0')
fb.fill(0x0)

# It is easy to write some text. The Framebuffer knows which
# pixels to set for the various letters (it has a simple
# built-in font)
fb.text( "Micro-", 0,10,1 )
fb.text( "   Ctrls" , 0,30,1 )
fb.text( "   in", 0,60,1 )
fb.text( " PADOVA", 0,90,1 )
fb.ellipse( 32, 93, 31, 10, 1)

# Now display the contents of the framebuffer:
oled.copyFramebuf( buf )

# Finally some blinking animation:
inv = False

while( True ) :
    if inv:
        inv = False
    else:
        inv = True
    oled.invert(inv)
    sleep(0.2)
    
# This little programme can be executed with a command like:
# (Do not forget to copy the class library for the display
# to the device before running this programme !)
#
# mpremote run testDisplay.py
#
