from machine import I2C, Pin
import framebuf
from time import sleep
from SH1107_OLED import OLED

OLED_ADR = const(0x3C)   # decimal: 60

scl = Pin( 32 )
sda = Pin( 33 )

i2c = I2C( 0,scl=scl,sda=sda,freq=100000)

######### Code to test the display

# If you want to use an external framebuffer uncomment these lines
# buf = bytearray( 128*16 )
# fb = framebuf.FrameBuffer( buf, 128, 128, framebuf.MONO_VLSB )

oled = OLED(i2c, OLED_ADR)
oled.init()
oled.setLandscape(True)


# If you use the framebuffer 
fb = oled.getFramebuffer()

# Erase the buffer (all black => all bits '0')
fb.fill(0x0)

fb.text( " PADOVA", 0,10,1 )
fb.ellipse( 32, 13, 31, 10, 1)

# Now display the contents of the framebuffer:
#oled.copyFramebuf( buf )
oled.copyFramebuf( )
