from machine import I2C, Pin
import framebuf
from time import sleep

OLED_ADR = const(0x3C)   # decimal: 60

##################################################################
class OLED :
##################################################################
    def __init__( self, i2c, i2c_adr ):
        self.i2c = i2c
        self.oled_adr = i2c_adr
        
    def _wr_cmd( self, cmd ):
        '''The data sheet explains how to send commands to the sh1107 chip on page
        15, section "Protocol". Commands are either 1 byte long or 2 bytes long.
        (See the command table.) After the address is transmitted a control byte
        has to follow. Bit 7 of this bytes ("Continue" bit) is one, it follows one
        data byte and the again a control byte. (Data byte here means "not control
        byte". It can be a command or a data byte for a command or a data byte for 
        the Pixel RAM). If the bit 7 of the control byte is 0, it means that there
        will only follow data bytes in the ongoing I2C transaction and no control
        byte anymore.
        The Bit 6 indicates if the next data byte is for the Pixel RAM ('1') or 
        not ('0'), which means it is a command or a data byte for the previous 
        command byte. 
        
        We send all the configuration bytes in separate I2C transactions (this is 
        simpler to program). Therefore we always send one Control Byte and one 
        data byte containing the command (or the data for the previous command 
        byte). 

        We need to write bytes to the i2c device. Integers can be converted to 
        bytes with the "to_bytes()" function in python. The first parameter ('1') 
        indicates that we want one single byte in the output. With the second 
        'parameter' you can choose if you want to have little- or big-endian 
        encoding, which means that in case you have big numbers with multiple bytes
        (we do not have them here) you can decide if the most signifcant bytes will
        come first or last in the output. (We put little endian here since the 
        esp32 is a little endian architecture, but since we only convert a single
        byte it is in fact irrelevant what we write here).
        
        The expression b'\x80 + cmd.to_bytes(1,'little') results in two bytes.
        First the 0x80 is sent out (the control byte) and then the cmd-byte.
        '''

        self.i2c.writeto( self.oled_adr, b'\x80'+ cmd.to_bytes(1,'little')  )

    def init(self):
        '''Initialize the display. This sequence comes from studying the datasheet.
        the sequence is very similar for many different controller chips on the 
        market. Hence if you get one working it is easy to adapt to another display.
        Sometimes some offsets have to be adjusted since this depends on how the 
        display is soldered to the controller chip.
        Btw: most of the parameters are correctly set after a Power On Reset (POR)
        Hence many of the commands below are strictly speaking not necessary. '''
    
        commands = [ 0xae,       # Turn display off during initialisation
                     0xd5,0x41,  # Sets oscillator freq and display clock divide ratio
                     0xa8,0x3f,  # Multiplex ratio to 0x3f
                     0xd3,0x60,  # Display offset (start line)
                     0xdc,0x00,  # Set display start-line to '0'
                     0x81,0x20,  # Set the contrast to 32

                     # The following values are already correct on power on.
                     # This is why they do not need to be set and the lines
                     # are commented out.

                     #0xa0,       # Scan direction (horizontal mirroring)
                     #0xc0,       # Vertical mirroring (c0 - c8)
                     #0xd9,0x22,  # Pre and dis-charge period (set to default)
                     #0xdb,0x35,  # Sets some display voltage (set to default)
                     #0xa4,       # Display RAM contents (not all white)
                     #0x00,       # Set column address to '0' (lower 4 bits)
                     #0x10,       # Set column address to '0' (higher 3 bits)
                     #0xb0,       # Set page address to '0'
                     #0x20,       # Address mode: page addressing
                     #0xa6,       # Display not inverted
                     #0xd5,0x41,  # Sets oscillator freq and clock divide ratio

                     0xad,0x80,  # Disable the internal DC/DC converter
                                 # (There is an external one)
                     0xaf        # Switch on the display
                                 ]

        for cmd in commands:
            self._wr_cmd( cmd )

        sleep(0.1)               # the data sheet wants this.
        

    def copyFramebuf(self, buf):
        '''copies a framebuffer to the Display so that the 
        contents gets displayed.'''
        
        for ip in range(0, 16):
            self._wr_cmd(0xB0 + ip) # page addr
            self._wr_cmd(0x00)      # col addr low
            self._wr_cmd(0x10)      # col addr high
                
            # here we should use a memoryview to avoid the
            # copying of the framebuffer every time.
            tmp = bytearray( 129 )
            tmp[0] = 0x40
            for i in range(0,128):
                tmp[i+1] = buf[ip*128+i]
            self.i2c.writeto( self.oled_adr, tmp )
            
    def invert( self, inv ):
        if inv:
            self._wr_cmd( 0xA7 )
        else:
            self._wr_cmd( 0xA6 )

########################################################################
# Now a small main-programme to display something on the OLED          #

scl = Pin( 32 )
sda = Pin( 33 )

i2c = I2C( 0,scl=scl,sda=sda,freq=100000)

# In this test program we check that we can find the device. This
# allows us to verify that we cabled up everything correctly.
i2cDevices = i2c.scan()

print( "List of I2C devices found during scan:" )

for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()

######### Code to test the display

oled = OLED(i2c, OLED_ADR)
oled.init()

# Micropython has a Framebuffer class which does nice things
# and makes it simple to display text and simple graphics.
# Look at the documentation of this class !
# The memory allocation for the buffer has to be done by us
# and this memory is then passed into the constructor of the
# Framebuffer.
buf = bytearray( 128*16 )

fb = framebuf.FrameBuffer( buf, 128, 128, framebuf.MONO_VLSB )


# Erase the buffer (all black => all bits '0')
fb.fill(0x0)

# It is easy to write some text. The Framebuffer knows which
# pixels to set for the various letters (it has a simple
# built-in font)
fb.text( "Micro-", 0,10,1 )
fb.text( "   Ctrls" , 0,30,1 )
fb.text( "   in", 0,60,1 )
fb.text( " PADOVA", 0,90,1 )
fb.ellipse( 32, 93, 31, 10, 1)

# Now display the contents of the framebuffer:
oled.copyFramebuf( buf )

# Finally some blinking animation:
inv = False

while( True ) :
    if inv:
        inv = False
    else:
        inv = True
    oled.invert(inv)
    sleep(0.2)
    
# This little programme can be executed with a command like:
#
# mpremote run testDisplay_standalone.py
#
