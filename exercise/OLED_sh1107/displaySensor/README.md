In this example we go a step further. We use class libraries which contain components 
we develope in previous exercises. This will be useful also for future programs and 
instead of copying "spaghetti-code" into every new program we just use a library which
we have written once. 

To use the library it needs to be found when python encounters the "import" statement. 
The default search path for modules it the main directory of the micropython directory
and the subdirectory called "lib". Hence we install our libraries into the lib sub-
directory. Note that the lib subdirectory first has to be created.

To do this for the OLED display and the BME280 class-libraries we use the command

`
mpremote mkdir lib
mpremote fs cp SH1107_OLED.py :lib/
mpremote fs cp BME280 :lib/
`

Remember that destinations on the microcontroller (i.e. the remote system) are indicated
by a leading colon (:). You should see a progress bar during the copying. You can verify
that the file has arrived at the destination by using 

`mpremote fs ls :lib`

In order to run the programme with these class libraries you simply use the following:

`mpremote run displaySensor.py`

If you want to permantly install your programme so that it will be automatically exectuted
at power-up without that you need to have a serial console connected and need to type commands, 
you rename your programme to "main.py" and copy it into the root folder of the micropython 
environment in the microcontroller. 

`
cp displaySensor.py main.py
mpremote fs cp main.py :.
`

Do do some exercise you can explore the API of the framebuffer and draw some histogramms of
the evolution of the three different sensor values.

