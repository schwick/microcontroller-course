from machine import I2C, Pin
from SH1107_OLED import OLED
from BME280 import BME280
import framebuf
from time import sleep

BME_ADR  = const(0x77)
OLED_ADR = const(0x3C)



############################ main program ###############################

# Stay away from pins 34 ... 39 (only input) and the 6 pins connected
# to the internal Flaslsh of the WROOM module.
# Also do not use GPIO 0 since if pulled down during power up, the board
# will directly go into the bootloader and will not execute the loaded
# programme. The "boot" button on the board is connected to this pin and
# pull exactly this line low when pressed.
# In addition do not use the bootstrap pin GPIO 12: This pin changes the
# VDD_SDIO to 1.8V instead of the default value 3V3 if pulled up to '1'
# at power up. This prevents the development module to boot since it has
# a 3V3 Flash chip on board.
# All strapping Pins can be found on the PINOUT diagram (in pink)


# Here we choose to have separate I2C busses for the two devices.
# Since the devices have separate addresses we could also put them
# onto the same bus. It is a good exercise to do this. You save two
# pins which can be useful in some situations. 

# First we define the i2c interface for the BME280 sensor:
scl = Pin( 32 )
sda = Pin( 33 )

i2c = I2C( 1,scl=scl,sda=sda,freq=100000)
i2cDevices = i2c.scan()
print( "List of I2C devices found during scan of i2c1:" )
for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()

# or use the same i2c as the sensor
oled = OLED( i2c, OLED_ADR )
oled.init()
fb = oled.getFramebuffer()
bme = BME280( i2c )

print("go to loop")


# Read out the sensor in an endless loop
while( True ) :
    (T,P,H) = bme.doMeasure()
    A = bme.getAltitude()
    fb.fill(0x0)
    fb.text( "T:%5.2f" % T, 0,0,1 )
    fb.text( "P:%5.1f" % P, 0,20,1 )
    fb.text( "H:%5.2f" % H, 0,40,1 )
    fb.text( "A:%5.2f" % A, 0,60,1 )
    oled.copyFramebuf( )
    sleep(2)


