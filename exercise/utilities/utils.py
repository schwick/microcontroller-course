import json
import network
import time

class Config:
    
    def __init__(self, filename):
        try:
            fd = open( filename, 'r' )
            self.config = json.load( fd )
            fd.close()
        except Exception as e:
            print( "Could not read config file %s" % filename )
            print( str(e) )
            self.config = {}

    def exists( self, item ):
        if item in self.config:
            return True
        return False
        
    def get( self, item ):
        if self.exists( item ):
            return self.config[item]
        print("Item \"%s\" does not exist in configuration" % item)
        return None

    def items(self):
        return self.config.keys()

    
##################################################################################
def wifi_connect(ssid, password):
##################################################################################
    # get a "station interface" (opposed to access point interface) from the
    # netwrok library. This object has the magic methods to connect to the
    # wireless network and then to the LAN on the IP level.
    sta_if = network.WLAN( network.STA_IF )
    # If it is already active de-activate it first so that we always start
    # from the same base state.
    if  sta_if.active():
        sta_if.active(False)

    # Now try to connect to the WIFI
    sta_if.active( True )   # necessary to activate the interface.

    print( "Connecting...")
    sta_if.connect( ssid, password )

    # Poll to know when the connection succeeds

    connected = sta_if.isconnected()

    # create some dotted lines on the display to
    # indicate the process which takes time.

    while not connected:
        print( ".", end="")
        time.sleep(0.1) # this is 100ms
        connected = sta_if.isconnected()

    print()
    
    # If we arrive here we should be connected
    print( "Success !")
    mac = sta_if.config('mac')
    # Show the IP address we got from the DHCP server

    ifparm = sta_if.ifconfig()
    iptxt = "IP:%s" % ifparm[0]
    print( "We got the IP : %s" % ifparm[0] )

##################################################################################
