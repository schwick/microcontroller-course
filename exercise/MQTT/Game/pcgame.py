
##################################################################################
import time                           # standard package for timing              #
import json                           # we publish our data in json format       #
import random
import sys
sys.path.append( "../../utilities" )
from utils   import Config            # to read a configurattion file            #
import paho.mqtt.client as mqtt
##################################################################################

psuff = sys.argv[1]

players = {}
state = "idle"
startt = 0
nplayer = 0

def ticks_ms():
    return int(round(time.time()*1000))

def sleep_ms(n):
    time.sleep( n/1000.0)

def on_connect( client, userdata, flags, rc):
    print("Connected")

def on_subscribe( client, userdata, mid, granted_qos):
    print("subscribed")
    
##################################################################################
def mqtt_connect(client_id, mqtt_server):
##################################################################################

    # Now set up the connection to the MQTT Broker
    mqtt_client = mqtt.Client( client_id = client_id)
    #mqtt_client.username_pw_set( mqtt_user, mqtt_pwd )
    mqtt_client.on_connect = on_connect
    mqtt_client.on_subscribe = on_subscribe
    mqtt_client.connect( mqtt_server, keepalive=3600 )
    mqtt_client.loop_start()

        
    return mqtt_client
##################################################################################



def sub_cb( client, userdata, message):
    global players, client_id, startt, nplayer, state
    
    topic = message.topic
    msg = message.payload

    #print("Got callback with topic %s and payload %s" % (topic,msg))
    
    if state == "waitevt" : 
        if topic == "game/event":
            startt = ticks_ms()
            nplayer = 0
            print("now!")
            #input()
            rtime = random.uniform( 0.2, 0.8)
            time.sleep( rtime )
            dt = ticks_ms()-startt
            print( "%d ms"  % dt )
            
            res = { 'name' : client_id,
                    'time' : dt }
            mqtt.publish( 'game/result', json.dumps( res ) )
            nplayer = 0
            players = {}
            state = "waitresult"

        else:
            print( "state %s topic %s" % (state, topic) )

    elif state == "waitresult":
        if topic == "game/result":
            nplayer += 1
            result = json.loads( msg.decode() )
            players[result['name']] = result['time']
        else:
            print( "state %s topic %s" % (state, topic) )

    else:
        # if we come here there is a bug
        print( "state %s topic %s" % (state, topic) )
    
    if topic == "game/info":
        print("list of players")
        for p in players.keys():
            print("    %s" % p )
     



####################### Here the main programme starts ###########################
print("starting")
# read the configuration file
config = Config( "config_pcgame.json" )


# Connect to MQTT broker
client_id = config.get("client_id") + psuff 
mqtt = mqtt_connect( client_id, config.get("mqtt_server") )

# We now need to subscribe to the relevant topics on the mqtt network
mqtt.on_message = sub_cb

mqtt.subscribe( 'game/#' )
# Now we start out endless loop.

while True:
    state = "waitevt"
    wait = 1.0 - random.random()**nplayer
    wait =  int(2000 + 5000 * wait)
    cnt = wait
    while cnt > 0:
        sleep_ms(1)
        if state == "waitresult":
            break
        cnt-=1

    if state == "waitevt":
        mqtt.publish( "game/event", " ")
        while state == "waitevt":
            sleep_ms(1)

    # now we wait for 5 seconds 
    while ticks_ms() - startt < 5000:
        sleep_ms(1)
        
    # now all results should have arrived: display the ranking
    result = sorted( players.items(), key=lambda x : x[1] )
    for i in range(0, min(6,len(result))):
        dstr = "%4.2f %s" % (result[i][1]/1000.,result[i][0])
        print(dstr)
    time.sleep(5)

    print("#############")
