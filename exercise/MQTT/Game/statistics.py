#!/usr/bin/python3
import random

nplayer = 10
ndiv = 10
ngames = 10000

res = [0] * ndiv

scale = 30

def prob( nplayer ):
    x = 1 - random.random()**nplayer
    #x = random.random()
    return x

for i in range(0,ngames):
    x=2
    for j in range( 0,nplayer ):
#        nx = random.random()
        nx = prob(nplayer)        
        if nx < x:
            x = nx
    bin = int(x*ndiv)
    res[bin]+=1

print()
for i in range(0,ndiv):
    smax = int(res[i]/scale + 0.5)
    for j in range(0,smax):
        print( "*", end="")
    print()

print()
