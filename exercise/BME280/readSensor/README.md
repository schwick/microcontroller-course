Here we develop code to read out the BME280 sensor via I2C. The 
code contains calculations as described in the datasheet in order
to convert the raw values from the ADCs to real world measurements
of Temperature, Pressure and Humidity. As you can see there are
some interdependencies (i.e. the temperature is entering the
calculations of the Pressure and the Humidity.).

The code in readSensors.py prints out the calculations on the 
console.

Once the code for the readout of the sensor is written it would 
be nice to package it in a way, that it can be easily re-used in
other programs. For this the code is encapsulated in a Class 
which can be found in the parent-directory (BME280.py). The code
is the same as in the readSensor.py programme, however, packed
into a class which can easily be imported and re-used. Also a
small main program to test the class is included.

To use this class it needs to be installed onto the microcontroller
into a directory where micropython finds it. By default the search
path of micropython contains the main root directory and the sub-
directory "lib" if it exists. Therefore create the lib subdirectory
and transfer the file into it (using the mpremote utility).
