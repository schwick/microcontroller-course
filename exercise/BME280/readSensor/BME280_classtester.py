from machine import I2C, Pin
from time import sleep
from BME280 import BME280

# Stay away from pins 34 ... 39 (only input) and the 6 pins connected
# to the internal Flash of the WROOM module.

scl = Pin( 32 )
sda = Pin( 33 )

i2c = I2C( 0,scl=scl,sda=sda,freq=100000)
i2cDevices = i2c.scan()

print( "List of I2C devices found during scan:" )

for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()

# All the hard work is done in this class. Sensor specific commands
# and procedures are hidden from the user and the main program becomes
# simple and readable. (Compare this code to the non-object oriented
# version which incluse the entire program in one long program without
# using the concept of "encapsulation" concept of object oriented
# programming languages. 

bme = BME280( i2c )

while( True ) :
    bme.doMeasure()
    bme.dumpLastMeasurement()
    sleep(2)
