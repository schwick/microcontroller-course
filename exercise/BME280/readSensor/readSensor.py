from machine import I2C, Pin
from struct import unpack
import binascii
from time import sleep

# Define the following constants to access the registers in the BME280 chip
# Using the const expression saves memoy in the microcontroller
# The meaning of the various registers is explained in the data sheet.

BME_ADR = const(0x77)             # This is the I2C address of the chip

BME_REG_CHIP_ID     = const(0xD0) # This register holds an identification code (0x60)
BME_REG_RESET       = const(0xE0)

# Registers to define details on how the measurements should be performed and
# registers to trigger the actual measurement. 
BME_REG_CRTL_HUM    = const(0xF2) # 5 : oversampling 16
BME_REG_STATUS      = const(0xF3)
BME_REG_CTRL_MEAS   = const(0xF4)
BME_REG_CTRL_CONFIG = const(0xF5)

# Registers which hold the result of a measurement:
BME_REG_PRESS       = const(0xF7) # adr F7 ... F9 are 20 bits pressure (big endian)
                                  # F9 bits 4..7 are the most significant 4 bits)
BME_REG_TEMP        = const(0xFA) # adr FA ... FC contains the 20 bits for temperature
BME_REG_HUM         = const(0xFD) # adr FD and FE contain 16 bits of Humidity



# A helper function to read signed 16 bit calibration constants
# Multiple constants can be read from consecutive memory locations.
def addSignedCalibs( calib, adr, prefix, n, offset=1 ):
    for i in range(0,n):
        tmp = i2c.readfrom_mem ( BME_ADR, adr, 2 )
        adr += 2
        calib[prefix+str(i+offset)] = unpack('<h',tmp )[0]
    return calib

# Read the calibration data which has been programmed into the chip.
# Due to production tolerances not every sensor gives exactly the same
# value at a given temperature/pressure/humidity. At the factory every
# sensor is calibrated and the calibration constants are programmed
# into the chip (they cannot be changed afterwards). We read out these
# constants here, since we need them for some calculations later.
def readCalib():
    calib={}
    addSignedCalibs( calib, 0x88, 'T', 3 )
    tmp = i2c.readfrom_mem( BME_ADR, 0x8E, 2 )
    calib['P1'] = unpack('<H',tmp )[0]  # This one is unsigned !
    addSignedCalibs( calib, 0x90, 'P', 8, 2 )
    calib['H1'] = int( i2c.readfrom_mem( BME_ADR, 0xA1, 1 )[0] )
    addSignedCalibs( calib, 0xE1, 'H', 1, 2 )
    calib['H3'] = int( i2c.readfrom_mem( BME_ADR, 0xE3, 1 )[0] )
    tmp = i2c.readfrom_mem( BME_ADR, 0xE4, 2 )
    calib['H4'] = (int(tmp[0])<<4) + (int(tmp[1])&0xf)

    tmp = i2c.readfrom_mem( BME_ADR, 0xE5, 2 )
    calib['H5'] = (int(tmp[0]) & 0xF0 ) >> 4 + (int(tmp[1]) << 4 )

    calib['H6'] = int( i2c.readfrom_mem( BME_ADR, 0xE7, 1 )[0] )

    return calib


# The formulas of the following calculations come from the data sheet.
#
# Calculate the Temperature with help of the calibration data
def calcTemp( adc_T, calib ):
    var1 = ( ( ( (adc_T>>3) - (calib['T1']<<1) ) * calib['T2'] ) ) >> 11
    var2 = (((((adc_T>>4) - (calib['T1'])) * ((adc_T>>4) - (calib['T1']))) >> 12) * (calib['T3'])) >> 14
    t_fine = var1 + var2
    T = (t_fine * 5 + 128) >> 8
    return T,t_fine

# Calculate the pressure with help of the calibration data
# This calculation includes a temperature correction.
def calcPress( adc_P, calib, t_fine ):
    # uses the alternate formula of the datasheet since there are no
    # 64bit integers on the esp32 port of micropython (the esp32 is
    # a 32 bit machine)
    var1 = t_fine - 128000;
    var2 = var1 * var1 * calib['P6'];
    var2 = var2 + ((var1*calib['P5'])<<17);
    var2 = var2 + ((calib['P4'])<<35);
    var1 = ((var1 * var1 * calib['P3'])>>8) + ((var1 * calib['P2'])<<12);
    var1 = ((1<<47)+var1)*(calib['P1'])>>33;

    if (var1 == 0):
        return 0;
    # avoid exception caused by division by zero }
    p = 1048576-adc_P;
    p = int( (((p<<31)-var2)*3125)/var1 );
    var1 = ((calib['P9']) * (p>>13) * (p>>13)) >> 25;
    var2 = ((calib['P8']) * p) >> 19;
    p = ((p + var1 + var2) >> 8) + ((calib['P7'])<<4);
    return p/25600

# Calucalate the humidity with help of the calibration data
# This calculation includes a temperature correction 
def calcHum( adc_H, calib, t_fine ):
    
    v_x1_u32r = t_fine - 76800;
    v_x1_u32r = (((((adc_H << 14) - ((calib['H4']) << 20) - ((calib['H5']) * v_x1_u32r)) + 16384) >> 15) * (((((((v_x1_u32r * calib['H6']) >> 10) * (((v_x1_u32r * calib['H3']) >> 11) + 32768)) >> 10) + 2097152) * calib['H2'] + 8192) >> 14))
    v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * calib['H1']) >> 4))
    if v_x1_u32r < 0 :
        v_x1_u32r =  0
    if v_x1_u32r > 419430400 :
        v_x1_u32r = 419430400
    return(v_x1_u32r>>12)


# Initialize the sensor for single measurements between sleeps.
# The maximum amount of oversampling is requested to achieve maximal precision.
def initSensor():

    # Set the chip to sleep mode
    # Set the pressure oversampling to 16
    # Set the temerature oversampling to 16
    i2c.writeto_mem( BME_ADR, BME_REG_CTRL_MEAS, b'\xB4' )
    
    # Set the oversampling of the pressure measurement to 16
    i2c.writeto_mem( BME_ADR, BME_REG_CRTL_HUM, b'\x05' )  


# Do the measurements of Temperature, Pressure and Humidity
def doMeasure( calib ):
    # Leave the oversampling values as defined in the init routine but wake up the chip into "forced mode".
    # This means the chip is exactly performing one measurement and then returns to sleep mode.
    i2c.writeto_mem( BME_ADR, BME_REG_CTRL_MEAS, b'\xB5' )

    # Here we wait until the measurement is done.
    # The bit 3 is '1' when a measurement is ongoing. It goes to '0' once the
    # measurement is completed and results are ready for reading out. 
    measuring = 8
    while measuring == 8:
        sleep(0.1)
        measuring = int(i2c.readfrom_mem( BME_ADR, BME_REG_STATUS, 1 )[0]) & 8


    # Now we read out the measurements. The values are raw values which need to
    # be transformed via formulas into Temperature, Pressure and Humidity values.
    # The formulas involve calibration constants. In addition the raw measurement
    # value depend on each other (i.e. the raw values for humidity and pressure
    # are temperature dependent. This dependency is known and worked into to the
    # forumalas for the calculation.

    # read temperature
    temp = i2c.readfrom_mem( BME_ADR, BME_REG_TEMP, 3 )
    #print( 'raw temp ', binascii.hexlify(temp) )
    T = int(temp[0]<<12)+int(temp[1]<<4)+int(temp[2]>>4)
    newT, t_fine = calcTemp( T, calib )
    print( "Temperature : %7.2f C" % (newT/100.))

    
    # read pressure
    press = i2c.readfrom_mem( BME_ADR, BME_REG_PRESS, 3 )
    #print( 'raw press ', binascii.hexlify(temp) )
    P = int(press[0]<<12)+int(press[1]<<4)+int(press[2]>>4)
    newP = calcPress( P, calib, t_fine )
    print( "Pressure    : %7.2f mb" % newP)

    # read humidity
    hum = i2c.readfrom_mem( BME_ADR, BME_REG_HUM, 2 )
    #print( 'raw hum ', binascii.hexlify(hum) )
    H = int(hum[0]<<8)+int(hum[1])
    newH = calcHum( H, calib, t_fine )
    print( "Humidity    : %7.2f %%" %(newH/1024.))

    print()


    
# Stay away from pins 34 ... 39 (only input) and the 6 pins connected
# to the internal Flash of the WROOM module.

scl = Pin( 32 )
sda = Pin( 33 )

i2c = I2C( 0,scl=scl,sda=sda,freq=100000)
i2cDevices = i2c.scan()

print( "List of I2C devices found during scan:" )

for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()

# Now read the chip Id
chipId = i2c.readfrom_mem ( BME_ADR, BME_REG_CHIP_ID, 1 )

print("The chip ID is 0x%02x" % int(chipId[0]) )
print()


# Read out the calibration constants from the chips memory.
# They are needed for the calculation of the temperature,
# pressure and humididty values.
calib = readCalib()
# initialise the sensor
initSensor()
# Read out the sensor in an endless loop
while( True ) :
    doMeasure(calib)
    sleep(2)
