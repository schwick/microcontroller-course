For this exercise:

We use the module with I2C readout. Hence we leave the CS pin unconnected.
We just connect the power pins (since we use 3.3V use the pins 3V and GND.
Double check before you connect the board to power!) and SCK and SDI for
the I2C data transfer. We connect these pins as follows:

Pins chosen:

BME280    |    ESP32
--------------------
SCL       |    GPIO 32
SDI       |    GPIO 33


The Green LED on BME board should lighten up

Now we perform a I2C scan to see if the board is recognised:

source ~/mc-python/bin/activate
minicom -D /dev/ttyUSB0

>>> from machine import I2C
>>> from machine import Pin
>>> i2c = I2C( 0, scl=Pin(32), sda=Pin(33), freq=100000)
>>> i2c.scan()
[119]
>>> 

(If y see the 119 at the end everything seems to work and we can start
to play with the module)
