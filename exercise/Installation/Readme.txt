Command to save the content of the 4MB flash. This could be used to save the original
contents of the flash when the module comes from the provider and before we
re-programme it with micropython.

esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 read_flash 0x0 4194304 out.flash

