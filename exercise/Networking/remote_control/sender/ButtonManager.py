from Button import Button
from time import sleep
import uasyncio


# The ButtonManager handles a group of buttons. Buttons are defined by the call to "addButton"
# For the button group named sets of Actions can be defined: these are callbacks for all or selected
# buttons of the group.The actions sets can be activated to adapt the actions for the buttons to a
# new situation. The Actions sets can also be handled in a Stack where they can be pushed and popped.
# Finally the is a coroutine to poll the buttons defined. (Buttons are defined with no interrupt).
class ButtonManager:

    def __init__( self ):
        self.bh = {}
        self.actionStack = []
        self.currentActions = {}
        self.actionSet = {'default' : {}}
        
    def addButton( self, PinNo, name, clickcb = None, downcb = None ):
        self.bh[name] = { "button" : Button( PinNo, name, clickcb, downcb, noirq = True ) }
        self.currentActions[name] = { "click" : clickcb,
                                     "down"  : downcb }
        self.actionSet['default'][name] = self.currentActions[name]
        #print("action set now ", self.actionSet)
        
    def addAction( self, actionSetName, buttonName, actions ):
        if actionSetName not in self.actionSet:
            self.actionSet[actionSetName] = {}
        self.actionSet[actionSetName][buttonName] = actions
        #print( "after addaction ", self.actionSet)

    def activateActionSet( self, name ):
        self.currentActions = self.actionSet[name]
        print( "Activating BM action set %s" % name, self.actionSet[name])
        self.activate( self.actionSet[name])
        
    def activate( self, actions ):
        for name,acts in actions.items():
            self.bh[name]['button'].resetCBs()
            for a,cb in acts.items():
                setattr(self.bh[name]['button'], a, cb)
            
    def pushActionSet( self, name ):
        self.actionStack.append( self.currentActions )
        self.currentActions = self.actionSet[name]
        self.activate(self.currentActions)

    def popActionSet( self ):
        self.currentActions = self.actionStack.pop()        
        self.activate(self.currentActions)

    async def pollButtons( self, dt=0.1 ):

        while True:
            for name,but in self.bh.items():
                but["button"].changed()

            await uasyncio.sleep( dt )
        
if __name__ == "__main__":
    class Menu:
        def down(self, but, ct):
            print('down')
        def up(self, but, ct):
            print("up")
        def enter(self, but,x):
            print("enter")
            bm.pushActionSet( "sub" )
        def exit(self, but, ct):
            print("exit")
            bm.popActionSet()

    m = Menu()
    bm = ButtonManager()
    bm.addButton(12, "one", m.down )
    bm.addButton(14, "two", m.enter )

    bm.addAction( "sub", "one", { 'click' : m.up } )
    bm.addAction( "sub", "two", { 'click' : m.exit } )
                  
    while True:
        sleep(1)
