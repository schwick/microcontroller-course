import time
from machine import Pin, PWM, SPI, ADC
from ST7567 import ST7567
import uasyncio

tasks = []

async def measureP( ):
        
    while True:

        adcval = []
        for adc in adcarr :
            adcval.append(0)

        nmeas = 100
        for i in range(0,nmeas):
            ix = 0
            for adc in adcarr:
                adcval[ix] += adcarr[ix].read_uv() / 1000
                #adcval[ix] += adcarr[ix].read_u16()
                ix += 1

        y = 5
        for val,name in zip(adcval,names):
            val /= nmeas
            lcd.rect( 4,y-1,120,9,1,True)
            lcd.text( "%s %4.1f mV" % (name, val), 10,y,0) 
            #lcd.text( "ADC %d" % val, 10,y,1) 
            y += 20
        lcd.show()
        await uasyncio.sleep(0.3)

spi = SPI(1,baudrate=100000, polarity=1, phase=1, sck=Pin(23), mosi=Pin(22),miso=None) #under 20Mhz is OK

lcd=ST7567(spi,a0=Pin(18),cs=Pin(21),rst=Pin(5),elecvolt=0x18,regratio=0x05,invX=False,invY=True,invdisp=0)

names = ["Pwr","Thr","Rud"]

adcPower  = ADC( Pin(36) )
adcRudder = ADC( Pin(34) )
adcThrust = ADC( Pin(39) )

adcarr = [adcPower, adcThrust, adcRudder]

bl = Pin(19, Pin.OUT)
bl.value(1)

lcd.fill(0)
lcd.rect( 0,0,128,64,1,False)

lcd.show()


async def main():

    poti_task = uasyncio.create_task(measureP())
    tasks.append(poti_task)

    rets = await uasyncio.gather( poti_task)
    print("rets", rets)
    
    
time.sleep(2)
print("run forever")
uasyncio.run(main())
print("done")
sock.close()
