#!/usr/bin/python3

import socket
from time import sleep
import json
import re
import websockets
import asyncio
from pprint import pprint

server = "192.168.4.1"
port = 80
s = False


# This test program opens a websocket which is used by the crtl.html webpage
# to send data to this server and then this server transfers the data to a
# esp32 based remote control receiver.
#
# All out of date.

def sendit( s, b ):
    try:
        #print(str(s))
        s.sendall( b )
        return s
    except Exception as e:
        print ("exception %s" % e)
        connected = False
        while not connected:
            try:
                s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                print("connect to ", server, port)
                s.connect( (server, port) )
                print("connected, now sending")
                s.sendall(b)
                print("success")
                connected = True
                return s
            except Exception as e:
                print("%s" % e)
                sleep(0.2)
                return False
            


#ic = 0
#servo_vals = [30, 77, 125]
#nvals = len(servo_vals)
#while True:
#    #sval = int(input("value:"))
#    istr = input("instr:")
#    mo = re.match( r"c(\d) (\d+)", istr)
#    if not mo:
#        print("syntax error : %s" % istr)
#        continue
#    c = int(mo.group(1))
#    sval = int(mo.group(2))
#    
#    doc = { "cmd" : "servo","chan" : c,"val" : sval}
#    docstr = json.dumps( doc ) + "\n"
#    print( docstr )
#    b = bytearray()
#    b.extend( map(ord,docstr) )
#    s = sendit( s, b )

#
#while True:
#    ix = ic % nvals
#    sval = servo_vals[ix]
#    doc = { "cmd" : "servo","chan" : 0,"val" : sval}
#    docstr = json.dumps( doc ) + "\n"
#    b = bytearray()
#    b.extend( map(ord,docstr) )
#    s = sendit( s,b )
#    sleep(2)
#    doc = { "cmd" : "servo","chan" : 1,"val" : sval}
#    docstr = json.dumps( doc ) + "\n"
#    b = bytearray()
#    b.extend( map(ord,docstr) )
#    s = sendit(s, b)
#    sleep(2)
#    ic+=1

async def handler(websocket, path):
    global channel, volume, s
    while True:        
        data = await websocket.recv()
        print( "received data : ",data)
        reply = f"Data received as:  {data}!"
        if data == "Connection Established":
            continue
        try:
            data = json.loads( data )
            pprint(data)
            b = bytearray()
            docstr = json.dumps( data ) + "\n"
            b.extend( map(ord,docstr) )
            s = sendit( s, b )

        except Exception as e:
            print (e)
            print (data)
#            
#        cmd = data['cmd']
#        chan = data['chan']
#        val = data['val']
#        print( cmd, chan, val );
#        if cmd == "setWave":
#            # need to send 4 bytes after command (the last 2 are dummies)
#            out = ( [0xc4,  chan&0xff, (val>>8)&0xff, val&0xff] )
#            ser.write(out)
#            data = await websocket.recv()
#            #print( "received data ", data )
#            wave = json.loads( data )
#            #print( "sending ", repr(wave))

    
def wserror( ws, error ):
    print( ws )
    print(error)

def wsclose( ws, st, msg ):
    print( ws )
    print( st )
    print( msg)

async def wservermain():
    async with websockets.serve(handler, "localhost", 8000):
        await asyncio.Future()
asyncio.run( wservermain() )
