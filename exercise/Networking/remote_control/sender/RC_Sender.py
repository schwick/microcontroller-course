from utils import Config
from ST7567 import ST7567
from ButtonManager import ButtonManager
from Menu import Menu
from Calibrator import Calibrator
from machine import SPI, Pin, ADC
import network
import socket
import json
import uasyncio
from time import sleep

class RCSender:
    def __init__(self, menu, lcd, calt, calr ):
        self.menu = menu
        self.lcd = lcd
        self.calt = calt
        self.calr = calr
        self.config = Config( "data/RC_Config.json" )
        self.wifi_user = self.config.get("wifi_user")
        self.wifi_pass = self.config.get("wifi_pass")
        self.pwr_mod_scale = self.config.get("pwr_mod_scale")
        self.pwr_snd_scale = self.config.get("pwr_snd_scale")
        self.reader = False
        self.writer = False
        self.menu.bm.addAction( "arm", "red", { "click" : self.arm } )
        self.menu.bm.addAction( "arm", "navi", {} )
        self.menu.bm.addAction( "arm", "action", {} )

        self.menu.bm.addAction( "fly", "red", { "down" : self.disarm,
                                                "click" : self.disarm } )
        self.menu.bm.addAction( "fly", "navi", {} )
        self.menu.bm.addAction( "fly", "action", {} )
        self.tasks = {}

    def armDialog( self, but, ct, args ):
        self.lcd.fill(0)
        self.lcd.text( "Red button", 5,10,1)
        self.lcd.text( "for >0.5s", 5, 30, 1)
        self.lcd.show()
        self.menu.bm.pushActionSet("arm")
        
        
    def arm( self, but, para ):
        print( "in arm: para " , para)
        if para != "long":
            self.menu.bm.popActionSet()
            self.menu.display()
            return
        self.calt.forceZero()
        # Here we need to go in the active mode:
        # not sure: self.menu.bm.popActionSet() # we want to be one under the menu

        self.menu.bm.pushActionSet( "fly" )
        print("flying-mode: can disarm now")
        
        # asyncio info:
        # if using run here it returns but blocks the event loop (polling of
        # buttons does not work anymore). Probably run also waits for all
        # tasks created in the co-routine... see asyncio documentation of
        # the "normal python".
        # self.tasks['setupradio'] = uasyncio.create_task( self.setupRadio() )
        # we do not need a task handle since this task ends after the ap is setup
        uasyncio.create_task( self.setupRadio() )
        print("radio setup launched")

    async def waitDisconnect(self):
        # Check if there is a model connected
        self.connected = True
        while self.connected:
            if self.reader or self.writer:
                self.lcd.fill(0)
                self.lcd.text("Disconnect model", 0,10,1)
                self.lcd.text("first !",40,20,1)
                self.lcd.show()
                if not self.calt.forceZeroFlag:
                    print("Abort disarm procedure: somebody toggled zeroforce")
                    self.lcd.fill(0)
                    self.lcd.show()
                    return
            else:
                self.connected = False
            await uasyncio.sleep(0.3)

        # Now continue to disconnect
        # only the server task needs to be killed
        if "server" in self.tasks:
            self.tasks['server'].cancel()
            del self.tasks['server']
            print("deleted server task")
        self.menu.bm.popActionSet()
        self.menu.bm.popActionSet()
        self.menu.display()
        print("all should be fine now")
        
    def disarm( self, but, para ):
        print("in disarm with %s %s" % (but, para))
        # force throttle to 0
        if para == "click":
            self.calt.toggleZero()
            return

        if para != "long":
            return

        self.calt.forceZero()
        uasyncio.create_task( self.waitDisconnect() )
        
        # kill receiver and sender loop tasks
        # disable the radio (if not model connected)
        # self.menu.bm.popActionSet()
        # self.menu.display()
        
    async def setupRadio(self):
        ap = network.WLAN( network.AP_IF )
        ap.active( False)
        ap.config( ssid = self.wifi_user, key = self.wifi_pass, security=3)
        ap.active( True )
        self.ap = ap
        while ap.active() == False:
            sleep(0.1)

        self.lcd.fill(0)
        self.lcd.text("My IP:", 5,10,1)
        self.lcd.text("%s" % ap.ifconfig()[0], 5,20,1)
        self.lcd.show()
        print( ap.ifconfig() )

        self.server = uasyncio.start_server( self.conncb, "0.0.0.0", 80)
        print("server coroutine done")
        self.tasks["server"] = uasyncio.create_task( self.server )
        self.lcd.text("Connection wait", 5,40,1)
        self.lcd.show()
        print("setupRadio ends")
        
    async def conncb( self, reader, writer ):
        if self.writer != False:
            print("Already someone connected... returning")
            await writer.drain()
            writer.close()
            reader.close()
            await writer.wait_closed()
            await reader.wait_closed()
            return

        print("Some model wants to connect.")
        self.reader = reader
        self.writer = writer        
        self.peer_info = reader.get_extra_info( "peername" )
        self.lcd.fill(0)
        self.lcd.text( "Model connected:",5,10,1)
        self.lcd.text( self.peer_info[0],20,25,1)
        self.lcd.show()

        ok = False
        while not ok:
            tval = self.calt.getValue(ignoreForce=True)
            print("tval %d" % tval)
            if tval < 1480 or tval > 1520:
                lcd.rect(0,38,128,23,1, True)
                lcd.text("Throttle to OFF",5,40,0)
                lcd.text("%4d" % tval, 45,50,0)
                lcd.show()
                await uasyncio.sleep( 0.3 )
            else:
                ok = True
                lcd.rect(0,38,128,23,0, True)
                self.lcd.show()
                # really arm the thing
                self.calt.forceZero( False )

        print("Starting loops")

        self.setupFlyDisplay()
        self.tasks["sendLoop"]    = uasyncio.create_task(self.sendLoop())
        self.tasks["receiveLoop"] = uasyncio.create_task(self.receiveLoop())
        self.tasks["statusLoop"]  = uasyncio.create_task(self.getStatus())

        print("Model fully connected")

    def setupFlyDisplay(self):
        self.lcd.fill(0)
        self.lcd.rect(0,0,128,64,1,False)
        self.lcd.vline(62,0,64,1)
        self.lcd.vline(65,0,64,1)
        self.lcd.rect(10,0,45,10,0,True)
        self.lcd.text("Model",10,0,1)
        self.lcd.rect(71,0,50,10,0,True)
        self.lcd.text("Sender", 71,0,1)
        self.lcd.show()
        
    async def receiveLoop( self ):
        connected = True
        while connected:
            try:
                line = await self.reader.readline()
                tele = json.loads( line )
                if "rssi" in tele:
                    lcd.rect( 5,38,52,12,1,True)
                    tstr = "%4ddb" % tele['rssi']
                    lcd.text( tstr,8,40,0)
                if "pwr" in tele:
                    lcd.rect( 5,48,52,12,1,True)
                    pwr = tele['pwr'] * self.pwr_mod_scale
                    tstr = "%4dmV" % pwr
                    lcd.text( tstr,8,50,0)


                lcd.show()
                
            except Exception as e:
                print("read connection lost %s" % str(e))
                connected = False
                ### here we should shut down ###
                self.reader.close()
                await self.reader.wait_close()
                self.writer.close()
                await self.writer.wait_close()
                self.reader = False
                self.writer = False
                self.tasks["statusLoop"].cancel()
                del self.tasks["statusLoop"]

        del self.tasks["receiveLoop"]
                

    async def getStatus( self ):

        while True:
            adcval = self.pwr_snd_scale * float(adcPower.read_uv()) / 1000.0 
            lcd.rect( 68,50,59,10,0,True)
            tstr = "%4dmV" % adcval
            #print("status: %s" % tstr)
            lcd.text( tstr,75,50,1)

            lcd.rect( 68,30,59,10,0,True)
            tstr = "R:%4d" % rval
            lcd.text( tstr,75,30,1)
            
            lcd.rect( 68,20,59,10,0,True)
            tstr = "T:%4d" % tval
            lcd.text( tstr,75,20,1)

            lcd.show()
            await uasyncio.sleep(1)
    
    async def sendLoop( self ):
        global tval,rval
        connected = True
        while connected:
            await uasyncio.sleep(0.1)
            try:
                tval = self.calt.getValue()
                rval = self.calr.getValue()
                
                self.writer.write( '{"servo":[%4.0d,%4.0d]}\n' %
                                   (tval,rval))
                await self.writer.drain()
                
            except Exception as e:
                print("send loop: ", str(e))
                print("Connection reset by model")
                self.lcd.fill(0)
                self.lcd.text("Lost model!", 20,30,1)
                self.tasks["statusLoop"].cancel()
                del self.tasks["statusLoop"]
                await self.writer.drain()
                await self.reader.drain()
                self.writer.close()
                self.reader.close()
                await self.writer.wait_closed()
                await self.reader.wait_closed()
                self.reader = False
                self.writer = False
                self.lcd.show()
                connected = False
        del self.tasks["sendLoop"]
        return


if __name__ == "__main__":
    print("Main: test RC_Sender")
    rval = 0
    tval = 0
    
    spi = SPI(1,baudrate=100000, polarity=1, phase=1,
              sck=Pin(23), mosi=Pin(22),miso=None) #under 20Mhz is OK
    lcd=ST7567(spi,a0=Pin(18),cs=Pin(21),rst=Pin(5),
               elecvolt=0x18,regratio=0x05,
               invX=False,invY=True,invdisp=0)
    bl = Pin(19, Pin.OUT)
    bl.value(1)
    lcd.fill(0)
    #lcd.rect( 0,0,128,64,0,True)
    lcd.show()

    bm = ButtonManager()
    bm.addButton( 12, "action")
    bm.addButton( 14, "navi" )
    bm.addButton( 27, "red" )
    
    
    menu = Menu( lcd, bm, "navi", "action" )
    calt = Calibrator( "Thrust", lcd, 39, menu )
    calr = Calibrator( "Rudder", lcd, 34, menu )
    adcPower  = ADC( Pin(36) )

    rc = RCSender(menu, lcd, calt, calr)

    menu.addItem( "Main", "Settings" )                  
    menu.addItem( "Main", "Calib Thrust", 1 )
    menu.addItem( "Main", "Calib Rudder", 1 )
    menu.addItem( "Main", "Arm", 0, rc.armDialog, True )

    menu.display( "Main" )
    
    # start the event loop by polling the buttons
    uasyncio.run(bm.pollButtons())

    print("We should never come here: we are about to leave main")

