import string
from time import sleep

class Spinner :
    def __init__( self, startval, digits, charset, disp ):
        self.disp = disp
        self.value = startval
        self.digits = digits
        self.charset = charset
        self.charix = charset.index( startval[0] )
        self.cdigit = 0

    def nextDigit(self):
        if self.cdigit == self.digits-1:
            self.cdigit = 0
        else:
            self.cdigit += 1
        self.charix = self.charset.index( self.value[self.cdigit] )

    def prevDigit( self ):
        if (self.cdigit == 0) and (self.digits > 0):
            self.cdigit = self.digits-1
        elif self.cdigit == 0:
            return
        else:
            self.cdigit -= 1
        self.charix = self.charset.index( self.value[self.cdigit] )

    def spinUp( self ):
        self.charix = ( self.charix + 1 ) % len( self.charset )
        tmp =  list(self.value)
        tmp[self.cdigit] = self.charset[self.charix]
        self.value = ''.join( tmp )
        
    def spinDown( self ):
        self.charix = ( self.charix - 1 ) % len( self.charset )
        tmp =  list(self.value)
        tmp[self.cdigit] = self.charset[self.charix]
        self.value = ''.join( tmp )
        
    def getVal(self):
        return self.value

    def getIntVal( self ):
        return int( self.value )

    def getFloatVal( self ):
        return float( self.value )
    

if __name__ == "__main__":
    sp = Spinner( str(153), 3, "0123456789", True )
    for i in range(0,10):
        print( "%03d" % sp.getIntVal() )
        sp.spinUp()
        sleep(0.4)
        
    print("---------")
    sp.nextDigit()
    for i in range(0,9):
        print( "%3d" % sp.getIntVal() )
        sp.spinDown()
        sleep(0.4)

    print("---------")
    sp.nextDigit()
    for i in range(0,9):
        print( "%3d" % sp.getIntVal() )
        sp.spinUp()
        sleep(0.4)

