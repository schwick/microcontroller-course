from machine import Pin, mem32
import struct

DEBUG = const( False )

# Configure the pulse counter in a way that it increments or decrements
# depending of the state of the control signal at the moment the pulse
# comes to the counting pin.

# ATTENTION the clock to the pulse counter unit needs to be enabled
# and the reset must be removed in the following register

DPORT_PERIP_CLK_EN_REG  = const(0x3FF000C0)  # bit 10 for PCNT (default is 0)
DPORT_PERIP_RST_EN_REG  = const(0x3FF000C4)  # bit 10 for PCNT (should be 0 by default)

# We want to use channel 0 of unit 0
# pcnt_sig_ch0_in0  : signal 39 
# pcnt_ctrl_ch0_in0 : signal 41

#  GPIO_FUNCy_IN_SEL_CFG  = const(0x3FF44130 + signal*4)
GPIO_FUNCy_IN_SEL_CFG_REG = const(0x3FF44130)
GPIO_SIGy_IN_SEL          = const(0x00000080)
GPIO_FUNCy_IN_SEL         = const(0x00000000)


#GPIO_FUNCx_OUT_SEL_CFG = const(0x3FF44530 + 4*x) x is the GPIO pad 0 to 39
GPIO_FUNCx_OUT_SEL_CFG  = const(0x3FF44530)
GPIO_FUNCx_OEN_SEL      = const(0x00000400)

GPIO_ENABLE_REG         = const(0x3FF44020)  # GPIOs  0 to 31
GPIO_ENABLE1_REG        = const(0x3FF4402C)  # GPIOs 32 to 39


# Pulse counter registers
PCNT_U0_CONF0_REG       = const(0x3FF57000)
PCNT_U0_CONF1_REG       = const(0x3FF57004)
PCNT_U0_CONF2_REG       = const(0x3FF57008)

PCNT_U0_CNT_REG         = const(0x3FF57060)

PCNT_CTRL_REG           = const(0x3FF570B0)
PCNT_U0_STATUS_REG      = const(0x3FF57090)

CONF0_CH0_ROT_CONFIG    = const(0x00440600)

def pmem32( adr, dat, name = "no name", op="" ):
    if op == "":
        mem32[adr] = dat
    elif op == "or":
        mem32[adr] |= dat
    elif op == "and":
        mem32[adr] &= dat
    else:
        print("UNKNOWN OPERATION: %s" % op )

    if DEBUG:
        check = (mem32[adr] + (1<<32)) & 0xFFFFFFFF
        print( "%30s : mem32 adr 0x%08x   dat 0x%08x   check 0x%08x" %
               (name, adr, 0xffffffff & (dat+(1<<32)), check))

class Rotary:
    def __init__(self, p1, p2, unit = 0):

        if unit > 7:
            print("ERROR: Unit must be in the range 0..7)")
            return
        
        # Set up the pins as input. This should setup the GPIO MUX to
        # select the GPIO Matrix???.
        self.p1 = Pin(p1,Pin.IN, None)
        self.p2 = Pin(p2,Pin.IN, None)


        # Route the GPIO inputs to the pulse counter functions
        # Make sure there is not output enables.
        
        # use the channel 0 input 0 of the pulse counter.
        # func 1 is input, func2 is control
        func1 = 39 + unit*4 

        func2 = 41 + unit*4
        if unit > 4:
            func1 += 14
            func2 += 14

        
        
        gpio_func_in_sel_cfg_1 = GPIO_FUNCy_IN_SEL_CFG_REG + 4 * func1
        gpio_func_in_sel_cfg_2 = GPIO_FUNCy_IN_SEL_CFG_REG + 4 * func2

        cfg1 = GPIO_SIGy_IN_SEL + p1
        cfg2 = GPIO_SIGy_IN_SEL + p2

        pmem32( gpio_func_in_sel_cfg_1, cfg1, "gpio_func_in_sel_cfg_1" )
        pmem32( gpio_func_in_sel_cfg_2, cfg2 ,"gpio_func_in_sel_cfg_2" )

        gpio_funcx_out_sel_cfg_1 = GPIO_FUNCx_OUT_SEL_CFG + 4 * p1
        gpio_funcx_out_sel_cfg_2 = GPIO_FUNCx_OUT_SEL_CFG + 4 * p2

        # oen forced to follow bits in GPIO_enable_regs
        pmem32( gpio_funcx_out_sel_cfg_1, GPIO_FUNCx_OEN_SEL, "gpio_funcx_out_sel_cfg_1" )
        pmem32( gpio_funcx_out_sel_cfg_2, GPIO_FUNCx_OEN_SEL, "gpio_funcx_out_sel_cfg_2" )
                
        gpio_enable_reg_1 = GPIO_ENABLE_REG
        gpio_enable_reg_2 = GPIO_ENABLE_REG
        if p1 > 31 :
            gpio_enable_reg_1 = GPIO_ENABLE1_REG
        if p2 > 31 :
            gpio_enable_reg_2 = GPIO_ENABLE1_REG

        if p1 < 32:
            gpio_enable_data_1 = 1 << p1
        else:
            gpio_enable_data_1 = 1 << (p1 - 32)
        if p2 < 32:
            gpio_enable_data_2 = 1 << p2
        else:
            gpio_enable_data_2 = 1 << (p2 - 32)

        pmem32( gpio_enable_reg_1, ~gpio_enable_data_1, "gpio_enable_reg_1", "and")        
        pmem32( gpio_enable_reg_2, ~gpio_enable_data_2, "gpio_enable_reg_2", "and")


        ########### PULSE COUNTER ###############
        
        # not sure what to do with the control register.
        # read it out and print what's in it at startup
        # release the reset of the counter:
        pmem32( PCNT_CTRL_REG,  0x5554)

        # Now setup the pulse counter chan0 input0
        # First enable the clock to the PCNT peripheral        
        pmem32( DPORT_PERIP_RST_EN_REG,0xFFFFFBFF,"DPORT_PERIP_RST_EN_REG", "and")
        pmem32( DPORT_PERIP_CLK_EN_REG,0x00000400,"DPORT_PERIP_CLK_EN_REG","or")

        # Configure the counter of unit 0 channel 0
        pmem32( PCNT_U0_CONF0_REG, CONF0_CH0_ROT_CONFIG, "PCNT_U0_CONF0_REG")

        # finally reset the counter before starting
        self.reset()
        
    def read(self):
        res =  mem32[ PCNT_U0_CNT_REG ]
        val = struct.unpack("<h",res.to_bytes(2,'little'))[0]
        if DEBUG:
            print( "cnt adr 0x%08x val 0x%08x %d" % (PCNT_U0_CNT_REG, val,val))
        return val
    
    def reset(self):
        pmem32( PCNT_CTRL_REG,  0x5555)
        pmem32( PCNT_CTRL_REG,  0x5554)

