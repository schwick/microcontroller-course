import network
import time
import socket
import json
from machine import Pin, PWM, SPI, ADC
from ST7567 import ST7567
from Rotary import Rotary
import uasyncio

server = "192.168.4.1"
port   = 80

print("starting")

tasks = []

async def measureV():
    while True:
        adcval = adc.read_uv() / 1000 * 0.00716
        adcvalt = adcT.read_uv() / 1000
        lcd.rect( 48,0,80,10,0,True)
        lcd.text( "Vbat %3.2fV" % adcval, 48,0,1)        
        lcd.rect( 48,30,80,10,0,True)
        lcd.text( "Vtst %3.2fV" % adcvalt, 48,30,1)        
        lcd.show()
        await uasyncio.sleep(1)

async def sendRotary():
    while True:
        rotval = roti.read()
        #print("rotval %d" % rotval)
        rotval += 77
        lcd.rect( 48,40,80,10,0,True)
        lcd.text( "Rot %d" % rotval, 48,40,1) 
        lcd.show()
        ret = sendChannel( 0, rotval )
        await uasyncio.sleep( 0.05 )
        
async def measureP():
    while True:
        adcval = 0
        for i in range(0,10):
            adcval += adcP.read_uv() / 1000
        adcval /= 10
        lcd.rect( 48,20,80,10,0,True)
        lcd.text( "Pot %3.2f mV" % adcval, 48,20,1) 
        lcd.show()
        c=0.17
        min=30
        val = int((adcval - 500) * c + min)
        if val < 0:
            val = 0
        val += min
        #print(int(val))
        ret = sendChannel( 1,val )
        lcd.rect(30,50,30,10,0,True)
        lcd.text("%3d" % val, 30,50,1 )
        await uasyncio.sleep(0.05)

#rot_flag = uasyncio.ThreadSafeFlag()

#async def rotary():
#    while True:
#        await rot_flag.wait()
#        uasyncio.create_task( update_rotary() )
        
        
async def rssi():
    while True:
        rssi = st.status('rssi')
        lcd.rect( 48,10,80,10,0,True)
        lcd.text( "RSSI %3.0fdB"%rssi, 48,10,1)
        lcd.show()
        await uasyncio.sleep(2)
        
    
def sendChannel( channel, value ):
    try:
        doc = { "cmd" : "servo", "chan" : int(channel), "val" : int(value)}
        docstr = json.dumps( doc ) + "\n"
        ba = bytearray( docstr.encode() )
        sock.sendto( ba, ( "192.168.4.1", 80 ) )

        # scale limits
        smax = 115
        smin = 40
        delta = 115-40+1
        deltapix = 64
        center = 77

        reclen =  int(abs(value-center)*(deltapix/delta)+0.5)
        if value < center:
            y0 = int(deltapix/2)
        else:
            y0 = int(deltapix/2) - reclen
        x0 = channel * 15
        
        lcd.rect( x0,0,10,64,0,True );
        lcd.rect( x0,0,10,64,1, False );
        lcd.rect( x0,y0,10,reclen, 1, True)
        lcd.line(0,31,25,31,1)
        lcd.show()
            
        return True
    
    except Exception as e:
        print( "Exception in sendChannel : %s " % repr(e) )
        return False

#spi = SPI(1,baudrate=100000, polarity=1, phase=1, sck=Pin(32), mosi=Pin(14),miso=Pin(26)) #under 20Mhz is OK
spi = SPI(1,baudrate=100000, polarity=1, phase=1, sck=Pin(32), mosi=Pin(14),miso=None) #under 20Mhz is OK

lcd=ST7567(spi,a0=Pin(27),cs=Pin(25),rst=Pin(33),elecvolt=0x18,regratio=0x05,invX=True,invY=False,invdisp=0)

#rotatry decoding

#roti = Rotary( 36, 13, rot_flag )
roti = Rotary( 26, 13 )

    
adc = ADC( Pin(39) )
adcP = ADC( Pin(34) )
adcT = ADC( Pin(35) )

lcd.fill(0)
lcd.rect( 0,0,128,64,1,False)

lcd.text("Connect WLAN", 0, 0, 1)
lcd.show()

st = network.WLAN( network.STA_IF )
st.active( False )
time.sleep( 0.5 )
st.active( True )
st.connect( "student12", "$unilab1" )

while not st.isconnected():
    time.sleep(0.2)
    lcd.text( "no connect yet", 0,15, 1)
    lcd.show()
lcd.rect( 0,15,128,15,0,True)
lcd.text("ok", 112, 0, 1)
lcd.show()    
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
lcd.text( "udp socket    ok", 0, 15, 1)
lcd.show()

lcd.fill(0)

ret = sendChannel( 0, 77 )
ret = sendChannel( 1, 77 )



async def main():

    vmeas_task = uasyncio.create_task(measureV())
    poti_task = uasyncio.create_task(measureP())
    rotary_task = uasyncio.create_task(sendRotary())
    rssi_task  = uasyncio.create_task(rssi())
    #rotary_task = uasyncio.create_task( rotary() )

    tasks.append(vmeas_task)
    tasks.append(rssi_task)
    tasks.append(poti_task)

    rets = await uasyncio.gather( vmeas_task, poti_task, rssi_task, rotary_task )
#    rets = await uasyncio.gather( vmeas_task, poti_task, rssi_task)
    print("rets", rets)
    
    
time.sleep(2)
print("run forever")
uasyncio.run(main())
print("done")
sock.close()
