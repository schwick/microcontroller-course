class MenuItem:
    def __init__(self, menu, txt, mtype, action=None, *args):
        self.txt = txt
        self.menu = menu
        self.type = mtype
        self.actioncb = action
        self.args = args
        
    def action( self, but, para ):
        if para != "click":
            return
        if self.type == 1:
            self.menu.enterSubMenu( self.txt )
        elif self.type == 0:
            print("menuitem action %s" % self.txt)
            if self.actioncb:
                self.actioncb( but, para, self.args )
    
    def exit( self ):
        pass
            
class Menu:
    def __init__(self, lcd, buttonManager, naviBut, actionBut):
        self.items = {}
        self.tft = lcd
        self.bm = buttonManager
        self.xmin = 5
        self.ymin = 8
        self.ydelta = 10
        self.ndisp = 5
        self.ysel = -1
        self.sitem = 0
        self.ditem = 0
        self.amenu = ""
        self.menustack = []
        self.bm.addAction('default', naviBut, {'click' : self._navigate } )
        self.bm.addAction('default', actionBut, {'click' : self._itemAction } )
        self.bm.activateActionSet('default');



    def addItem( self, menu, text, mtype=0, action=None, *args ):
        if menu not in self.items:
            self.items[menu] = []
        self.items[menu].append( MenuItem( self, text, mtype, action, *args ) )
        return self.items[menu]


    def pushMenuStack( self ):
        self.menustack.append( (self.amenu, self.sitem, self.ditem) )
    
    def popMenuStack(self):
        if len(self.menustack) == 0:
            print("nothing to pop from menustack")
            
            return
        (menu,self.sitem,ix) = self.menustack.pop()
        self.display( menu,ix )



    def enterSubMenu( self, submenu ):
        self.pushMenuStack()
        self.sitem = 0
        self.display( submenu,0 )
    
    def display( self, menu=False, ix=-1 ):
        if ix == -1:
            ix=self.ditem
        if not menu:
            menu = self.amenu
        self.tft.fill(0)
        self.tft.rect( 0,0,128,64,1,False)
        self.ditem = ix
        self.amenu = menu
        y = self.ymin
        for i in range(ix, min(len(self.items[menu]), ix+self.ndisp) ):
            it = self.items[menu][i]
            self.tft.text( it.txt, self.xmin, y )
            y+=self.ydelta
        self._select( ) 
        self.tft.show()

    def _itemAction( self, but, para ):
        item = self.items[self.amenu][self.sitem]
        item.action(but, para)
        
    def _navigate( self, but, para ):
        if para == "click":
            self._next()
        elif para == "long": # long press navi: parent menu
            self.popMenuStack()

    def _exit( self ):
        print("exit")

    def _next(self):
        # At the end of the list wrap over
        if self.sitem + 1 >= len(self.items[self.amenu]):
            self.sitem = 0
            self.display( self.amenu, 0 )
            return

        # need to scroll: last is selected
        if self.sitem == self.ditem + self.ndisp - 1:
            self.sitem += 1
            self.display( self.amenu, self.ditem+1 )
            return

        # just select the next item
        self.sitem += 1
        self._select()
        self.tft.show()
        
        
    def _select( self ):

        if self.ysel >= 0:
            y = self.ysel*10 + self.ymin - 1
            self.tft.rect( self.xmin-2, y, 122, 9, 0 )
            
        self.ysel = self.sitem - self.ditem
        y = self.ysel*10 + self.ymin - 1
        self.tft.rect( self.xmin-2, y, 122, 9, 1 )
        self.tft.show()
        
