import time
from machine import Pin, PWM, SPI, ADC
from ST7567 import ST7567

print("starting")


#spi = SPI(1,baudrate=100000, polarity=1, phase=1, sck=Pin(32), mosi=Pin(14),miso=Pin(26)) #under 20Mhz is OK

spi = SPI(1, baudrate=100000, polarity=1, phase=1, sck=Pin(23), mosi=Pin(22), miso=None ) #under 20Mhz is OK

lcd=ST7567(spi,a0=Pin(18),cs=Pin(21),rst=Pin(5),elecvolt=0x18,regratio=0x05,invX=False,invY=True,invdisp=0)
#lcd=ST7567(spi,a0=Pin(18),cs=Pin(21),rst=Pin(5),elecvolt=0x18,regratio=0x05,invX=True,invY=False,invdisp=0)

bl = Pin(19, Pin.OUT)
bl.value(1)

lcd.fill(0)
lcd.rect( 0,0,128,64,1,False)

lcd.text("Hello Display", 10, 10, 1)
lcd.rect( 5,20, 118, 10, 1)
lcd.rect( 10,23, 108, 4, 1)
lcd.show()

i=0
a=1
c=1
while True:
    #print("run forever", i)
    lcd.ellipse( 64, 48, 10+2*i, 10, c);
    i+=a
    lcd.show()
    if ((i >= 25) or (i == 0)):
        a=-1*a
        c=(c+1)%2
print("done")
