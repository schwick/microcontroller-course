from machine import Pin
from time import ticks_ms, ticks_diff

# A class to hande a button connected to a GPIO pin of the ESP32.
# The pin is configured in this class (as input with a pull-up).
# The button is assumed to connect the input to ground if pressed.
# The class is able to distinguish "click", "long-click" and
# "button-down" events. Callbacks can be registered for button-down
# events and for click (and long-click) events. Click and long click
# events call the same callback with a parameter allowing to distinguish
# the two. 
# The button can be operated in Interrupt mode or in polling mode.
# If interrupts mode is chosen (default) the internal interrupt
# handler Button.changed is called: it finds out the event and calls
# the registered callbacks if they exist.
# If polling mode (opposed to interrupt mode) is chosen the user
# has to call the "changed" routing during polling.

class Button :
    def __init__( self, Pinno, name, clickcb=None, downcb=None, noirq=False ):
        self.pin = Pin(Pinno, Pin.IN, Pin.PULL_UP)
        if not noirq:
            self.pin.irq( trigger = Pin.IRQ_FALLING | Pin.IRQ_RISING,
                          handler=self.changed)
        self.name = name
        self.value = self.pin.value()
        self.press_t = ticks_ms()
        self.release_t = ticks_ms()
        self.click = clickcb
        self.down = downcb
    
    def resetCBs(self):
        self.click = None
        self.down = None
        
    def poll(self):
        return self.pin.value()

    def changed(self,p=False):
        val=self.pin.value()
        if val == self.value:
            #print("prelled")
            return
        
        self.value = val
        ts = ticks_ms()
        
        if self.value == 0:
            if ticks_diff( ts, self.release_t ) < 10:
                 #print("prelled cp")
                 return
            self.press_t = ts            
            if self.down:
                self.down( self, "down" )
        else:
            if ticks_diff( ts, self.press_t ) < 10:
                 #print("prelled cr")
                 return
            self.release_t = ts
            if ticks_diff( self.release_t, self.press_t ) < 500:
                #print( "%s click %s"%(self.name, str(self.click)))
                if self.click:
                    #print("button %s click" % self.name)                    
                    self.click( self, "click" )
            else:
                if self.click: 
                    #print("button %s longclick : execute \"%s\"" % (self.name, self.click.__name__))
                    self.click( self, "long" )
                    #print("The click returned")
