#!/usr/bin/python3

import socket
from time import sleep
import json
import re

def sendit( s, d ):
    try:
        #print(str(s))
        s.sendall( b )
        return s
    except Exception as e:
        print ("exception %s" % e)
        connected = False
        while not connected:
            try:
                s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                print("connect to ", server, port)
                s.connect( (server, port) )
                s.sendall(b)
                print("success")
                connected = True
                return s
            except Exception as e:
                print("%s" % e)
                sleep(0.2)
                return False
            

server = "192.168.4.1"
port = 80
s = False

ic = 0
servo_vals = [30, 77, 125]
nvals = len(servo_vals)
while True:
    #sval = int(input("value:"))
    istr = input("instr:")
    mo = re.match( r"c(\d) (\d+)", istr)
    if not mo:
        print("syntax error : %s" % istr)
        continue
    c = int(mo.group(1))
    sval = int(mo.group(2))
    
    doc = { "cmd" : "servo","chan" : c,"val" : sval}
    docstr = json.dumps( doc ) + "\n"
    print( docstr )
    b = bytearray()
    b.extend( map(ord,docstr) )
    s = sendit( s, b )


while True:
    ix = ic % nvals
    sval = servo_vals[ix]
    doc = { "cmd" : "servo","chan" : 0,"val" : sval}
    docstr = json.dumps( doc ) + "\n"
    b = bytearray()
    b.extend( map(ord,docstr) )
    s = sendit( s,b )
    sleep(2)
    doc = { "cmd" : "servo","chan" : 1,"val" : sval}
    docstr = json.dumps( doc ) + "\n"
    b = bytearray()
    b.extend( map(ord,docstr) )
    s = sendit(s, b)
    sleep(2)
    ic+=1
