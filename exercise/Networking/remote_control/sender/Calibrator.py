from Menu import MenuItem
from time import sleep, sleep_ms
from machine import ADC, Pin
from micropython import schedule
import uasyncio
import os
import json

class Calibrator (MenuItem):
    def __init__( self, qt, lcd, adcpin, menu ):
        self.adc = ADC( Pin(adcpin) )
        self.quantity = qt
        self.lcd = lcd
        self.menu = menu
        self.menu.bm.addAction("calib"+qt, "action", {"click" : self._calibaction})
        self.menu.bm.addAction("calib"+qt, "navi", {})
        self.adcpin = adcpin
        self.calibstate = None
        self.calibrationfn = "/data/calib_" + qt + ".dat"
        self.forceZeroFlag = False
        
        # load calbration if it exists.
        self.calib = self.loadCalib()
        if not self.calib:
            # use default calib
            self.calib = {
                "minimal pos"    : 100.0,  # mV
                "maximal pos"    : 1000.0, # mV
                "zero pos"       :  450,    # zero val being 1.5ms
                "minimal value"  :  500,    # us
                "maximal value"  : 2500 }   # us        
            print("Using default calibration since no calib file found")
            self.saveCalib()

        print("Constructor calib is ", self.calib)
        self.calstop = False # used to finish calibration
        # define the menuitems for the calib actions
        menu.addItem( "Calib %s"%qt, "Set Max Value", 0, self.calibtrigger, "maximal value")
        menu.addItem( "Calib %s"%qt, "Set Min Value", 0, self.calibtrigger, "minimal value")
        menu.addItem( "Calib %s"%qt, "Set Max Pos", 0, self.calibtrigger, "maximal pos")
        menu.addItem( "Calib %s"%qt, "Set Min Pos", 0, self.calibtrigger, "minimal pos")
        menu.addItem( "Calib %s"%qt, "Set Zero Pos", 0, self.calibtrigger, "zero pos")
        menu.addItem( "Calib %s"%qt, "Reset (default)", 0, self.calibtrigger, "default")
        menu.addItem( "Calib %s"%qt, "Dry test", 0, self.calibtrigger, "test")

        self.calibStartEvt = uasyncio.ThreadSafeFlag()
        self.calibStartEvt.clear()
        #print("calibstartevt", self.calibStartEvt)
        self.calibIdleEvt = uasyncio.ThreadSafeFlag()
        self.calRun = True
        self.calibIdleEvt.set()
        #print("creating task calibrate")
        self.ctask = uasyncio.create_task(self.calibrate())
        #print("contructor Calibrator done", self.ctask)

    def loadCalib( self ):
        try:
            fd = open( self.calibrationfn, 'r' )
            self.calib = json.load( fd )
            print("success found:", self.calib )
            fd.close()
            return self.calib
        except:
            print("Could not find file")
            return False

        
    def saveCalib( self ):
        try:
            dstat = os.stat( "data" )
        except:
            os.mkdir("data")
        
        fd = open( self.calibrationfn, 'w' )
        json.dump( self.calib, fd )
        fd.close()
        print("Calibration saved")
        
    def calcPulseWidth( self, u, fac=1 ): 
        cal = self.calib
        self.slope_pos = fac * ((cal["maximal value"] - 1500)    / ( cal["maximal pos"] - cal["zero pos"] ) )
        self.slope_neg = fac * ((1500    - cal["minimal value"]) / ( cal["zero pos"] - cal["minimal pos"] ) )

        if cal["maximal pos"] < cal["minimal pos"] :
            tmp = self.slope_pos
            self.slope_pos = self.slope_neg
            self.slope_neg = tmp         

        # also clip values to nin and max if fac==1 (i.e. we do not run in calibration mode)
        if u > cal["zero pos"]:
            pw = 1500 + (u-cal["zero pos"])*self.slope_pos
        else:
            pw = 1500 + (u-cal["zero pos"])*self.slope_neg

        if fac == 1:
            pw = max( pw, cal["minimal value"] )
            pw = min( pw, cal["maximal value"] )
        return pw
            
    async def calibrate( self ):
        #print("in calibrate %s" % self.quantity)
        while True:
            #print("wait calibStartEvt", self.calibStartEvt)
            await self.calibStartEvt.wait()
            #print("Calibrate starts", self.calibStartEvt)

            if self.calibstate == "default":
                self.calib = {
                    "minimal pos"  : 100.0,  # mV
                    "maximal pos"  : 1000.0,  # mV
                    "zero pos"     : 450, # zero val being 1.5ms
                    "minimal value"  : 1000,  # ms
                    "maximal value"  : 2000 } # ms
            else:
                self.menu.pushMenuStack()
                self.lcd.fill(0)
                self.lcd.text( "Put %s to" % self.quantity,5,5,1)
                self.lcd.text( "  %s" % self.calibstate,5,15,1)
                self.lcd.text( "Press enter",5,30,1)
                self.lcd.text( "  when done",5,40,1)
                self.lcd.show()
                
                self.menu.bm.pushActionSet( "calib"+self.quantity )
                self.calRun = True
                while self.calRun:
                    val = 0
                    for i in range(0,64):
                        val += self.adc.read_uv() / 1000
                    val /= 64
                    self.lcd.rect(63,52,63,10,1,True)
                    self.lcd.rect(0,52,127,10,1,True)
                    if (self.calibstate == "test" ):
                        pval = self.calcPulseWidth( val )
                        self.lcd.text( "%4.0fms" % pval, 80, 53, 0 )
                        self.lcd.text( "%6.1fmV" % val, 1, 53, 0 )
                    elif (self.calibstate == "maximal value") or (self.calibstate == "minimal value"):
                        val = self.calcPulseWidth( val, 2.0 )
                        self.lcd.text( "%4.0fms" % val, 70, 53, 0 )
                    else:
                        self.lcd.text( "%4dmV" % val, 70, 53, 0 )
                    self.lcd.show()
                    await uasyncio.sleep_ms( 300 )
                self.menu.bm.popActionSet()
                self.menu.popMenuStack()
            print("calibstate is ", self.calibstate)
            print("Here clib is ", self.calib)
            self.calib[self.calibstate] = val
            #print(self.calib)
            self.saveCalib()
            self.calibIdleEvt.set()

    def forceZero( self, force=True ):
        self.forceZeroFlag = force

    def toggleZero( self ):
        if self.forceZeroFlag:
            self.forceZeroFlag = False
        else:
            self.forceZeroFlag = True

        
    def getValue(self, ignoreForce=False):

        if self.forceZeroFlag and not ignoreForce:
            return 1500
        
        val = 0
        for i in range(0,64):
            val += self.adc.read_uv() / 1000
        val /= 64
        pval = self.calcPulseWidth( val )
        return pval
        
    def _calibaction(self, but, para ):
        # here we should save the calibration
        self.calRun = False
        
        
    # called from irq
    def calibtrigger( self, but, para, args ):
        #print("calibtrigger (%s): %s" % (self.quantity,args[0]))
        if para == 1:
            return
        self.calibIdleEvt.wait() # guarantees that calibstate is not overwritten during calibration
        #print("is idle")        
        self.calibstate = args[0]
        #print("trigger start evt")
        self.calibStartEvt.set()  # Trigger start of calibration
        #print("start trigger done")
        
