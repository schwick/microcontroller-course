from Menu import Menu
from ButtonManager import ButtonManager
from time import sleep
from ST7567 import ST7567
from machine import Pin,SPI
from Calibrator import Calibrator
import uasyncio

async def runmenu():
    print("in main")
    spi = SPI(1,baudrate=100000, polarity=1, phase=1, sck=Pin(23), mosi=Pin(22),miso=None) #under 20Mhz is OK
    lcd=ST7567(spi,a0=Pin(18),cs=Pin(21),rst=Pin(5),elecvolt=0x18,regratio=0x05,invX=False,invY=True,invdisp=0)
    bl = Pin(19, Pin.OUT)
    bl.value(1)
    lcd.fill(0)
    lcd.rect( 0,0,128,64,1,False)
    lcd.show()
    
    bm = ButtonManager()
    bm.addButton( 12, "action")
    bm.addButton( 14, "navi" )
    bm.addButton( 27, "red" )

    menu = Menu( lcd, bm, "navi", "action" )
    
    menu.addItem( "Main", "Calib Thrust", 1 )
    menu.addItem( "Main", "Calib Rudder", 1 )
    menu.addItem( "Main", "Arm" )

    calt = Calibrator( "Thrust", lcd, 39, menu )
    calr = Calibrator( "Rudder", lcd, 34, menu )

    menu.display( "Main" )
    
    while True:
        await uasyncio.sleep(2)

print("await main")
uasyncio.run( runmenu() )

print("menu exited!")

while True:
    sleep(2)
    print("testersleep")
