import time
from machine import Pin, PWM, SPI, ADC
from ST7567 import ST7567
from Button import Button

# Small test program for the Button class, testing the Button
# with IRQ and a callback (not used in the remote control: there
# we poll the buttons in a async task.

spi = SPI(1,baudrate=100000, polarity=1, phase=1, sck=Pin(23), mosi=Pin(22),miso=None) #under 20Mhz is OK

lcd=ST7567(spi,a0=Pin(18),cs=Pin(21),rst=Pin(5),elecvolt=0x18,regratio=0x05,invX=False,invY=True,invdisp=0)

bl = Pin(19, Pin.OUT)
bl.value(1)

lcd.fill(0)
lcd.rect( 0,0,128,64,1,False)

lcd.show()

abut = None
aclick = -1

def bcb( but, click ):
    global abut,aclick
    abut = but.name
    aclick = click


buty = Button( 12, "yellow", bcb )
butg = Button( 14, "green ", bcb )
butb = Button( 27, "blue  ", bcb )

while True:
    if abut != None:        
        lcd.rect( 10,10,100,10,0,True )
        lcd.text( "%s : %d" % (abut, aclick), 10,10,1)
        lcd.show()
        abut=None
        
    time.sleep_ms(200)
    
