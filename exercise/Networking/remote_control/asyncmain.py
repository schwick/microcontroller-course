import uasyncio
import network
import time
import select
import socket
import json
from machine import Pin, PWM, ADC
from utils import Config
import sys

dstval=[1500,1500,1500,1500]
actval=[1500,1500,1500,1500]
##################################################################################
def wifi_connect(ssid, password):
##################################################################################
    # get a "station interface" (opposed to access point interface) from the
    # netwrok library. This object has the magic methods to connect to the
    # wireless network and then to the LAN on the IP level.
    sta_if = network.WLAN( network.STA_IF )
    # If it is already active de-activate it first so that we always start
    # from the same base state.
    if  sta_if.active():
        sta_if.active(False)

    # Now try to connect to the WIFI
    sta_if.active( True )   # necessary to activate the interface.

    print( "Connecting...")
    sta_if.connect( ssid, password )

    # Poll to know when the connection succeeds

    connected = sta_if.isconnected()

    # create some dotted lines on the display to
    # indicate the process which takes time.

    while not connected:
        print( ".", end="")
        time.sleep(0.1) # this is 100ms
        connected = sta_if.isconnected()

    print()
    
    # If we arrive here we should be connected
    print( "Success !")
    mac = sta_if.config('mac')
    # Show the IP address we got from the DHCP server

    ifparm = sta_if.ifconfig()
    iptxt = "IP:%s" % ifparm[0]
    print( "We got the IP : %s" % ifparm[0] )
    return sta_if
##################################################################################


async def CommandLoop():

    while True:
        try:
            cmdbuff = await reader.readline()
        except Exception as e:
            print("Exception in readline: %s " % str(e))
            break
        if cmdbuff == None:
            print("did not get anything... strange")
            continue

        try:
            cmd = json.loads(cmdbuff)
        except Exception as e:
            sys.print_exception(e)
            print(cmd)
            continue
        if "servo" in cmd:
            ichan = 0
            for val in cmd["servo"]:
                dstval[ichan] = val
                ichan+=1


async def telemetry():
    while True:
        await get_tele()
        await uasyncio.sleep(1)

async def pwmLoop():
    max_step = config.get("max_step")
    while True:
        for ichan in range(0,2):
            delta = dstval[ichan]-actval[ichan]
            if delta > max_step:
                delta = max_step
            elif delta < -1*max_step:
                delta = -1*max_step
            actval[ichan] = actval[ichan]+delta
            channels[ichan].duty_ns( actval[ichan]*1000)
        await uasyncio.sleep(0.005)
        
async def get_tele():
    global reader, writer
    rssi = sta_if.status( 'rssi' )
    tstr = '{"rssi":%d}\n' % rssi

    # adc for power
    upwr = 0
    for i in range(0,16):
        upwr += adc_pwr.read_uv()  # in mV
    upwr /= 16000

    tstr =  '{"rssi":%d,"pwr":%d}\n' % (rssi,upwr)
    
    try:
        writer.write( tstr )
        await writer.drain()
    except Exception as e:
        sys.print_exception(e)
        writer.close()
        reader.close()
        await writer.wait_closed()
        await reader.wait_closed()
        print("closed socket, trying to re-connect")
        reader, writer =  await uasyncio.open_connection( server, 80 )
        print("connected again")
    

async def main():
    global sta_if
    global server
    global reader
    global writer
    global adc_pwr
    global config
    config = Config("data/RC_Config.json")
    user = config.get("wifi_user")
    password = config.get("wifi_pass")
    server = config.get("rc_server")

    sta_if = wifi_connect( user, password )

    reader, writer = await uasyncio.open_connection( server, 80 )
    
    # LED
    led = Pin(15, Pin.OUT)
    led.on()
    time.sleep(1)
    led.off()

    # ADC for power measurement
    adc_pwr = ADC( Pin(10),atten=1 )
    
    uasyncio.create_task(pwmLoop())
    uasyncio.create_task(telemetry())
    uasyncio.create_task(CommandLoop())

    while True:
        await uasyncio.sleep(10)
    
    s.close()
 

if __name__ == "__main__" :
    # These are needed in the global scope:
    # prepare the servo pwm
    pwm0 = PWM(Pin(16), 50, duty_ns=1500000)
    pwm1 = PWM(Pin(18), 50, duty_ns=1500000)
    channels = { 0 : pwm0,
                 1 : pwm1 }

    uasyncio.run(main())
    print("Main exited... this is weird")
    
