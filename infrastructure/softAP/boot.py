import network
import time

ap = network.WLAN( network.AP_IF )
ap.active( False)

ap.config( ssid = "student12", key = "$unilab1", security=3)

ap.active( True )

while ap.active() == False:
    time.sleep(0.1)
    print('.',end="")    
print()

print( ap.ifconfig() )
