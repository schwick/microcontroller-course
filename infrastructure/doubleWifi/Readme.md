# Set up a second Network on Laptop #

## Requirements ##

- iw : usually installed
- hostapd
- udhcpd

## Config steps ##

```
sudo iw phy phy0 interface add hotspot type __ap
ifconfig -a
sudo ifconfig hotspot 192.168.28.1 up
ifconfig -a
```
A configuration (hostapd.conf) file for the hostapd has to be created.

```
interface=hotspot
driver=nl80211
ssid=micro
channel=7
hw_mode=g
wme_enabled=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=3
wpa_passphrase=unipadova
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

Then you run 
```
sudo hostapd hostapd.conf
```

Finally set up the udhcpd with the config file as found in [3].
You have to edit the file and insert the ip address of the main wireless interface of the laptop, into the "opt router" field of the config file. (Btw the dns in the file are public google dns.)

Run the udhcpd daemon on the command line (for some reason it does not work as a service even though it is available as a service.)
```
sudo udhcpd -f
```
In /etc/mosquitto/conf.d/my_mosquitto.conf you change the contents to 

```
listener 1883 0.0.0.0
listener 1883 192.168.28.1
allow_anonymous true
```

This is enough to set up a wifi network for the course but there is no internet access from the "micro" network. For this to happen you need forwarding of packets and a nat firewall rule:

```
echo "1" > /proc/sys/net/ipv4/ip_forward
iptables --table nat --append POSTROUTING --out-interface wlo1 -j MASQUERADE
iptables --append FORWARD --in-interface hotspot -j ACCEPT
```


## References ##

[1]
[https://anooppoommen.medium.com/create-a-wifi-hotspot-on-linux-29349b9c582d](https://anooppoommen.medium.com/create-a-wifi-hotspot-on-linux-29349b9c582d) 

[2][https://wireles.wiki.kernel.org/en/users/documentation.iw](https://wireles.wiki.kernel.org/en/users/documentation.iw)

[3] [hostapd.conf](resource/hostapd.conf)

[4] [udhcp.conf](resource/udhcp.conf)