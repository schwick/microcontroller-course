from machine import UART
from time import sleep

br=9600
print("baudrate: ", br)
uart = UART(1, baudrate=br, tx=27, rx=14, timeout=999999 )


def conv( str, f ):
    if f == "N" or f == "S":
        deg = float( str[0:2] )
        minu  = float( str[2:] )
    else:
        deg = float( str[0:3] )
        minu = float( str[3:] )

    deg += minu/60.0
    return deg

def convTime( str ):
    hour = int(str[0:2])
    minu = int(str[2:4])
    seco = float(str[4:])
    return "%02d:%02d:%02.2f" % (hour, minu, seco)

while True:
    try:
        line = uart.readline()
        line = line.decode()
    except Exception as e:
        print( "read bad stuff ... probably UART not yet synced" )
        continue


    # process line
    toks = line.split(',')
    if toks[0] == "$GPGLL":
        latstr = toks[1]
        hemi = toks[2]
        longstr = toks[3]
        we = toks[4]
        timestr = toks[5]
        flag = toks[7]

        long = conv(latstr, hemi)
        lat = conv(longstr, we)
        
        print( "%02.5f %02.5f %s UTC" % (long, lat, convTime(timestr))  )
