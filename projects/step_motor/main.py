from machine import Pin, UART
from time import sleep_ms
import sys
import random
# Pins
# inputs 1/2/3/4 of the motor are on pins 36/39/34/35

pos = 0

p1 = Pin( 32, Pin.OUT )
p2 = Pin( 33, Pin.OUT )
p3 = Pin( 25, Pin.OUT )
p4 = Pin( 26, Pin.OUT )

pins = [p1,p2,p3,p4]

# Define an array of subsequent steps for each pin
# If we go through these steps in a sequence the motor
# will smoothly turn.

steparr = [ [0,0,0,1],
            [0,0,1,1],
            [0,0,1,0],
            [0,1,1,0],
            [0,1,0,0],
            [1,1,0,0],
            [1,0,0,0],
            [1,0,0,1] ]


# we have to limit the number of steps per second so
# that the motor can follow. Specify the step frequeny

step_freq = 1

##########################################################

istep = 0;

def do_n_steps( astep, nstep, freq, dir ):
    
    delay = int(1000/freq) # in ms
    while nstep > 0:
        astep = (astep+dir)%8
        for i in range(0,4):
            pins[i].value( steparr[astep][i] )
        nstep -= 1
        sleep_ms( delay )
    return astep

def rotate_degrees( astep, angle, freq ):
    global pos
    steps = abs(int(float(angle) / (5.625/64.0) + 0.5))
    print( "%4d steps for %4d degrees : pos = %6d" %(steps, angle, pos) )
    if angle > 0 :
        pos += steps
        astep = do_n_steps( astep, steps, freq, 1 )
    else:
        pos -= steps
        astep = do_n_steps( astep, steps, freq, -1 )
    return astep

# try to turn
print("hallo")
astep = 0
dir = 2
while True:
    angle = random.randint(-360,360 )
    astep = rotate_degrees( astep, angle, 1000 )
    sleep_ms( 300 )
