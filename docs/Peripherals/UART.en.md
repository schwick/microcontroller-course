# UART #
To be useful microcontrollers need to interact with their environment in some way. In most cases some data needs to be provided to the application running on the controller (e.g. some sensor values or some user input) and some processed data is coming out of the controller which has to be provided to some external hardware (motors, light, valves, switches, screens, LCD displays, amplifiers/speakers...).

A very simple and robust way to transfer data at moderate speed is the UART (Universal Asynchronous Receiver Transmitter). A UART transfers digital data by means of two wires: one is used for transmitting a bit stream, the other for receiving a bit stream (also a common ground line is usually required to be routed from sender to receiver). The transmitter and the receiver work independently, hence data can flow in both directions at the same time. This is called full-duplex operation. UART communication works between only two devices. 

![UART_connection](figures/uart_connection.webp)<br>
*Connection between two devices using UARTs to communicate with each other.*

A UART takes digital data in the form of bytes and serialises them so that one bit after the other is transferred over the line. A dedicated clock line is NOT needed, however, sender and receiver logic have to agree on a common bit rate (called baudrate) in order to successfully transfer data (i.e. the receiver has to know in advance how fast the bits are being transferred). How to agree on this baudrate is not specified in the protocol used by the UART. Usually the programmer has to choose a baudrate and configure the sender and the receiver accordingly. 

The following figure illustrates how the UART data transfer works:

![UART_communication](figures/UART_transmission.png)<br>
*Serial bitstream used during UART communication. Note that the clock is internally generated and used in the UART. It is NOT transferred from the transmitter to the receiver. Also the phase of the clock might be different in sender and receiver i.e. one clock might be inverted wrt to the other. The coloured lines, however, stay in their position. If a clock is inverted then the UART needs to use the opposite clock edges for the respective activities in the UART (change of data line level or sampling of the data line.)* 

Both UARTs involved in the communication generate a clock corresponding to the agreed baudrate. I.e. if the baudrate is 115200 a clock of 115.2 kHz is generated for use in the UART logic. (115200 is a typical baud rate used in the communication between microcontrollers and PCs.)

1. In the idle state (no data transfer ongoing) the sender keeps the TX line at '1'. 
1. To indicate the start of the communication the transmitter pulls down the data line for one clock cycle. The receiver detects this level change on the data line and synchronises its clock to this event. 
1. The example shows the transmission of eight data bits. The least significant bit is transferred first. In this example the transmitter always changes (if necessary) the data line level on the rising edge of its clock. The receiver always samples the data line on the falling edge of the clock to determine the level of the data bit. 
1. The transfer is finished with the data line pulled down for one clock cycle (the so called STOP bit). 

This simple protocol is very robust. As the following figure shows, it allows for significant differences in clock frequencies on the sender and receiver side. This makes it easy to implement the entire protocol in software. It is not difficult to program the UART communication in an ATMEL microcontroller running at 12MHz in assembler. 57600 bauds can be reached without problems. (This is very useful for simple ATMEL microcontrollers which do not have an embedded hardware UART on the chip.)

![UART_transmission_tolerant](figures/UART_transmission_tolerant.png)

*The UART transmission is tolerant for clock differences in transmitter and receiver.*

Another nice feature of this protocol is that the communication synchronises automatically between sender and receiver. If the receiver is powered up later than the transmitter and the transmitter is already sending data it just needs an idle time for more than 9 clock cycles to synchronise both sides to each other, since you can be sure that every data transfer is finished after 9 clock cycles. 

So far we only discussed one configurable parameter of the UART communication: the baudrate. There are three more options which have to be configured. It is possible to insert a parity before the stop bit. (The value of the parity bit is chosen such that the number of bits at '1' is always even.) This serves as a data consistency check. The parity check can detect (but not correct) any single bit flip and several other bit errors.
The number of stop bits is also configurable and can be one or two. Finally the number of data bits can be configured from 5 to 9. By far the most often used UART communication between PCs and microcontrollers is 8N1 which means 8 data bits, no parity bit and one stop bit. (In fact I have never seen a different configuration being used...). 
