# The I2C Bus

The I2C (Inter Integrated Circuit) bus has been developed by Philips in 1982 to connect low speed
peripherals to processors and microcontrollers on the same circuit board.
The design is based on simplicity, robustness and low cost.

## Hardware setup

The I2C bus requires just two lines to make processors talk to periphal
components. One line is used to transmit a clock and the second line is
used for serial data transfer in both directions: processor (=controller)
-> peripheral (=target) and the other way round). 

The following diagram shows the principle circuit of the I2C bus. In this
example only two devices (e.g. one Controller and one Target) are connected
to the bus.

![Electrical_circuit_of_an_I2C_bus](figures/I2C_electrical.png)

To understand the functioning of the bus the following has to be noted:
Both lines (SDA and SCL for data and clock) are "open drain" lines. This means that no device is allowed drive this line high (i.e. apply positive voltage to the line). The only thing the devices on the bus can do with either of the lines is to drive them "low" by switching on the MOSFET connected to the line. If the MOSFET is switched "on": effectively the line is connected to ground. If no device on the bus switches "on" their MOSFET the line will be on +VDD level through the pull-up resistor RP. In other words: each of the line can be switched to logical 0 by any of the connected devices. Only if NO device on the bus pulls the line low it will result as logic '1'. (This kind mechanism is also called "wired OR" since it implements a logical OR: if one or more devices switch on their MOSFET the result on the line is '0').

The diagram in addition shows that every device is monitoring the state of both lines (via the green driver which is then connected to the internal logic of the device (not shown in the diagram)).

There are different speed grades for I2C devices. The standard mode allows for bit rates up to 100kbit/sec. The "Fast Mode" goes up to 400kbit/s. These are the most common modes. Other modes exist for up to several Mbit/s. The capacitance of the SDA and SCL lines is limited to about 400pF which limits the length of the lines and the number of devices you can put on a line (every device has a little capacitance which will add to the total line capacitance). The capacity together with the value of the pullup resistor Rp is also a limiting factor for the achievable bit rate. However, it is always allowed for the controller to use arbitrarily low clock speeds.

## The operation of the I2C bus

The I2C bus is able to handle multiple controllers and multiple target devices. However, very often there is only one controller and a few target devices. The controller initiates communication with a target device by transmitting a 7 bit address on the bus followed by some register addresses or commands. The addressed target device is then responding by sending back the data using the same "SDA" line.
I2C target devices usually have a fixed address which is hardcoded into the device. But addresses on the same I2C bus have to be unique since only one target at a time is allowed to respond to the controller. To reduce the chance of address conflicts many target chips have the possibility to choose between two different hardware addresses. This allows to also operate two targets of the same type on the same bus. For cases where more than two targets with the same address have to be operated on the same bus, I2C multiplexer chips can be inserted into the bus structure. The following figure shows a diagram of such a multiplexer:

![I2C_Multiplexer](figures/I2CMultiplexer.png)

The multiplexer itself is an I2C target with its own address. This allows the controller to operate the internal switch which connects the I2C bus to different bus segments labelled S[D/C]0 ... S[D/C]7. 


## The I2C protocol

To understand how the I2C protocol works it is best to look at a typical example. However, some basic principles should be clarified before:

1. Any bus transaction (read or write procedures) can be split into frames: One address frame and one or more data frames. 
1. If the bus is idle (no activity) no device on the bus pulls the SDA or SCL clock low (so they are both high or '1'). In general the SDA line must not change when the SCL line is high. The exceptions to this rule are the Start and Stop conditions (see below). 
1. Each bus transaction starts with a so called "Start Condition". A start condition is created by the controller by pulling down the SDA line while leaving the SCL line high.
1. Any data transfer is achieved by transferring single bytes per frame. The MSB (Most Significant Bit, i.e. bit 7) is transferred first, the LSB (i.e. bit 0) is transferred last. To send a bit the sender (controller or target) pulls the clock line low and then adjusts the value of the SDA line to the bit-value which needs to be transferred. Then the SCL line is released (i.e. goes back to '1' via the pull up resistor). After this the receiving device samples the line (= reads the line) in order to determine if the transferred bit is '0' or '1'. 
1. After the transfer of 1 byte the RECEIVING device controls the SDA line for one clock cycle and has to pull the SDA line low for one cycle to acknowledge the reception of the byte.
1. When an entire transaction is finished (address frame and 1 or more data frames) the controller issues a "Stop Condition". This is done by creating a low to high transition on the SDA line while the SCL line is high.
1. If a controller wants to keep the control of the bus for multiple transactions it is possible to repeat the Start Condition right after the end of the previous transaction OMITTING the Stop Condition. 

The following figure shows an example of a typical transaction (we assume a temperature sensor is read). The controller wants to read the value of a register in a target.

![Typical_I2C_transaction](figures/I2CProtocolExample.jpg)

- The transaction starts with a Start Condition issued by the controller.
- Then follows the address frame: I2C addresses are 7 bit wide, however, we said that every frame transfers entire bytes (i.e. 8 bit). In the address frame the eighth bit is needed to indicate if the controller wants to write or read to or from the target in the subsequent data frames. Since after the address frame we want to write an identifier of the internal register we later want to read this R/W bit is set to '0' ('0' means write, '1' means read). The R/W bit is the LSB (bit 0). In the example the first address frame says the Controller wants to write to the target device with the address 0x48.
- The target acknowledges the reception of the address frame.
- It follows one data frame with the data 0x00. As said, in this example this data byte is interpreted by the target as an identifier of an internal register which the controller wants to read in subsequent accesses. This pattern is used in many (if not all) I2C target devices.
- The target acknowledges the reception of the data frame.
- The transaction is ended with a Stop Condition.
- The controller initiates a second transaction to the same target device, but this time it sets the LSB in the address frame meaning the controller wants to read data frames from the target.
- After the address frame the TARGET sends two bytes of data. Note that during read access it is the CONTROLLER which acknowledges each dataframe.
- The second byte is the last byte the controller wants to read. This is why the last byte is not acknowledged but the SDA is left on '1' in the 9th clock cycle of the data transfer. With this the controller signals to the target that it has read enough data. The controller then issues a STOP condition to end the transaction.

### 10 bit addressing

There is also a way to deal with 10 bit addresses of target devices. The address frame then consists of transferring 2 bytes. Targets with 10 bit addresses and 7 bit addresses can be mixed on the same bus. This is possible since the first byte transferred in the address frame of the 10 bit addressing cycle consists of a reserved bit combination which cannot be used for 7 bit addressing. (b'11110XXX'). There are some more bit combinations reserved in the addressing space of I2C so that in fact only 107 7-bit addresses are available for target addresses.

### Clock stretching

It might happen that the controller wants to read data from a target at a rate the target cannot sustain. In this situation the target has the possibility to slow down the data transfer. To do this the target just needs to hold down the clock line (which is normally controlled by the controller) as long as it needs. The controller then needs to wait until the target releases the clock line before it can proceed with the subsequent cycles. 

