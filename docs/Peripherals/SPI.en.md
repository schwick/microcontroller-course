# The SPI Bus

SPI (Serial Periphal Interface) is another wide-spread serial protocol to interconnect chips in embedded systems.

## The (hardware) lines of the SPI Bus

The figure below shows the lines required to implement a 4-wire SPI interface. 

![SPI Bus lines](figures/SPI_BUS_01.png)

As can be seen in the diagram there is one line providing a clock which is used to synchronize the data transfer. The line SS (Slave Select) is used to "activate" a slave: Only when this line is '0' the slave will "listen" on the other lines and take part in the transfer. The remaining two lines are used for the serial data transfer. As indicated by the names of the lines one is used to transfer data from the master to the slave (MOSI: Master Out, Slave In) and the other one to transfer data from the Slave to the Master (MISO: Master In, Slave Out). Data transfer can happen at the same time in both directions due to these two dedicated lines ("full duplex"). (However, this is not always used in practice. If, for example you write data into a tft-display the slave (=the display) will not send meaningful data back to the master (=microcontroller)

There exists a variant of the SPI bus only using three wires. In this case only one line for data transfer exists (generally the line is then called "SDA" (Serial Data)). This line can be used in one direction at a time, so data transfer is possible in both directions but only in one direction at a time. (This is called "half duplex".) Which direction is used depends on the design of the internal slave logic: For example to read some data from a register in the slave, the master will have to first transfer a specific command (depending on the architecture on the slave chip) to the slave and then it will receive the contents off the addressed register in the subsequent clock cycles.

It should be noted that there are no pull-up resistors present on the clock and data-lines. The communicating parties actively drive the lines high or low. This allows for higher data transfer rates as in the I2C protocol. 

### Working with multiple Slaves
By adding additional SS lines to the interface the master can address several slave chips re-using the same data transfer- and clock-lines. This is shown in the following diagram:

![Addressing multiple Slaves](figures/SPI_BUS_02.jpg)

Multiple SS lines on the master are used to activate only one Slave at a time (here the lines of the slaves are labelled "CS" meaning "Chip Select". This is a very common convention.). However, the designer of the board has to pay attention to the fact that the maximal possible clock frequency will be reduced in such a design since the capacity of each line (in particular the clock and the data transfer lines) increases when you add more slaves to the same line (the line becomes longer and in addition every chip-input adds capacitance to the line.)

## The SPI protocol

Compared to the I2C protocol, the SPI protocol is much simpler. No  addressing of the slave chip exists (since the slave chips are selected via the SS signal). In addition data transfers are not acknowledged and also start and stop bits do not exist. In the following diagram the transfer of data is shown. There exist four different ways to transfer data on a SPI bus. In general the architecture of the Slave chip is designed for one of these methods and the microcontroller has to be configured to use the correct transfer mode: 

![Addressing multiple Slaves](figures/SPI_timing_diagram_CS.jpg)

### Clock polarity
The polarity of the clock is defined by the state of the clock line when no data is transferred. If the clock line is '0' in the idle state the clock polarity is called '0' whereas if the clock line is '1' in the idle state the clock polarity is called '1'. 

### Phase CPHA
CPHA defines the transmission cycle of each bit wrt to the clock. These  transmission cycles define when (wrt to the clock cycle) the sending end has to change the value of the of the data transfer line to the value of the next bit to transfer, and when the receiving end has to sample the data transfer line in order to read the value of the bit being transferred. There are two possible transmission cycles:

   - CPHA = 0: The sender has to change the data line to the value of the next bit when the clock lines changes to the idle value. The first bit being transferred has to be put on the bus half a clock cycle BEFORE the clock is leaving the idle level. The receiving end on the other hand has to sample the data line when the clock line transitions from the idle value to the non-idle value. 
   - CPHA = 1: Is just the opposite of CPHA = 0. The sender has to change the data line to the value of the next bit when the clock lines changes from the idle value to the non-idle value. The first bit being transferred has to be put on the bus at latest at the same time when the clock line moves out of the idle value for the first time. The receiving end on the other hand has to sample the data line when the clock line transitions to the idle value. 

This mechanism makes sure that there is a half a clock cycle of time between the moment in which the data line is changed by the sender and when the data line is sampled by the receiver. This makes sure that the level of the data line has "settled" down to the correct value before it is being sampled (also at high clock speeds). Note that at high speeds this mechanism is very important since also with perfect signal integrity on the lines (i.e. perfect termination of the line, no "ringing" (=damped oscillations due to transmission line imperfection)) the propagation delay of the lines play an important role when trying to achieve high transfer rates: The clock signal is always generated in the Master. When the Master is transferring data to the Slave, data and clock travel on parallel lines. Therefore the clock transitions and the data line transitions are delayed by exactly the same amount of time and the relative position of these transitions to each other does not change. However, when the slave is receiving data the clock pulse is sent from the master to the slave. On the relevant clock edge (depending on the chosen CPOL and CPHA) the slave changes the value of the data line and the bit is then transferred back to the Master. This means the phase relation between clock transition of the outgoing clock from the master and data line transition when receiving a bit at the master changes by two time the propagation delay of the line between Master and slave. If you consider very high transition speeds of 100Mbit/s ~ 100MHz you have a half clock cycle of 5ns. This shows that the maximal propagation delay between Master and Slave can only be 2.5ns which are a few centimetres on a PCB board. (In reality things are worse since we assumed that everything else in the example is perfect, but this is never the case: there are imperfections in PCB board, tolerances in the components of the electronics involved, noise from the environment etc etc...) 

### Higher level protocols
The SPI protocol definition is very simple. No addressing concept is defined in the standard and also no data transfer length. (A data transfer can have any number of bits). Therefore: how the SPI protocol needs to be used with a specific chip has to be defined in the data sheet of the chip. 

Often chips are designed in a way that the first byte sent by the controller to the chip is decoded as a "command" and the following bytes are parameters (the number of bytes is specific to the command). This allows to define configuration commands where internally registers are set to values which are sent from the microcontroller. Similarly other commands define read-commands: when they are decoded the chips send data back to the master in the clock cycles following the decoding of the command. 

Sometimes SPI slaves have additional pins to help with the interpretation of the data. For example small SPI TFT displays often have a D/C pin which has to be set by the microcontroller to define if the transferred byte has to be interpreted by the display as data or as a command. This is convenient for TFT displays where often large amounts of pixel data have to be transferred as quickly as possible to the internal pixel memory of the display: a single command defines that all following data transfers go into subsequent locations of the pixel memory and then data can be transferred with a fast DMA in one go so that the entire display can be erased, or filled with a colour, or filled with data from an image.  
