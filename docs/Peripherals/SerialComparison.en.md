# Comparison of different serial interfaces #
 
The following table summarises the differences wrt. the I2C Bus discussed in the previous chapter. 

| item | UART | I2C | SPI |
|------|------|-----|-----|
| required wires | 2 | 2 | 3 (or 2 for half-duplex) for the  communication and one additional wire for each participating device to activate the the slave (Slave Select (SS) or Chip Select (CS)) |
| communication method | full duplex | half-duplex | full-duplex (however, also a half-duplex variant with three wires exist)|
| communication peers | 2 devices (no concept of controller and target) |multiple *controllers* and multiple *targets* | one *master* and one or multiple *slaves* |
| speed | typically up to 115200bits/s | 400kbit/s (some "exotic" modes up to some Mbit/s| up to 100Mbit/sec |
| hardware requirements | depends on implementation | pull-up resistors | none |
| Protocol features | No clock line. Transfer rate needs to be known by both communicating devices. Resynchronisation after each transferred data word (5-9 bits) | - supports clock-stretching to slow down the transfer<br/> - Overhead due to Start and Stop bits<br/> - Acknowledgement sent after each byte. | none of the I2C features are implemented in SPI.

("Full Duplex" means that data can be transferred in both direction simultaneously during the data transfer. "Half Duplex" means that at any point in time only one side can send data whereas the other side receives the data.)
