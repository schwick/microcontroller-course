# Typical Peripherals
To communicate with the outside world, any computer or controller needs peripherals. A specific feature of microcontrollers is that a number of peripherals are usually implemented on the chip. They allow to communicate with many electronic chips, sensors or boards like TFT displays, mini digital speaker systems, SD-cards. Some typical peripherals are discussed in this section. We will go into more detail in a dedicated chapter where we discuss the concrete usage of some of these interfaces. 

## Counters, Watchdog and RTC
Counters are useful to count events. For this the counting clock comes from some interface electronics which connects an input Pin of the controller (e.g. a switch or a photo-sensitive element). 

Gated counters can be used to  measure rate, velocity, revolutions per minute, etc. A gated counter counts for a well defined time called the gate (e.g. for one second), and then (if necessary) does some calculation on the count result to obtain the result in the desired unit. 


For time measurements microcontrollers often contain a reasonably precise internal clock which can be divided down to a commonly used interval like μs, ms or s. This allows to do timing measurements or implement wall clocks. 

A watchdog is a special counter which is often used to "work around problems". A watchdog is set to a pre-defined value and then starts counting backwards to 0. If 0 is reached something happens. Often the system is brute-force rebooted. To avoid the re-boot the application on the microcontroller has to periodically load the watch dog counter with the pre-defined start value using a dedicated command. The idea behind this is that the microcontroller system might recover on its own in case of a crash due to a software bug or another problem. Of course this quick and dirty approach is not suitable for anything mission critical...

A RTC (real time clock) is simply a dedicated counter which counts with a human friendly clock (e.g. milliseconds) and which contains several registers to maintain the wall clock (i.e. time of the day) and the date.

## UART
UART stands for Universal Asynchronous Receiver/Transmitter. It is a standard communication interface to transfer digital data using only two serial lines. This peripheral is present in all microcontrollers and usually is the main interface used during programming and debugging. Given its importance it will be discussed in a dedicated chapter. UARTs can be interfaced to modern computers by using a small converter which converts the serial data stream to a usb data stream. In Linux the data stream is then accessible via serial device files like /dev/ttyUSB0 or /dev/ttyACM0. (In Windows these are the COMx interfaces.)

## USB
In some more recent microcontrollers a USB interface is implemented on the chip. It normally can be configured to implement a host or a device interface. In these controllers the USB interface is usually used to interface to the host computer in order to program or debug the device. But the USB interface can be used by the microcontroller application for any other purpose of course. 

## I2C
I2C is a simple serial interface used to establish the communication between the microcontroller and one or multiple peripherals chips. The communication requires only two wires. This interface will be handled in a dedicated section later. 

## SPI
Like I2C SPI is another serial protocol used to make a microcontroller communicate with additional external peripherals. 4 wires are needed to implement an SPI interface. SD cards can be read and written via an SPI interface. Similarly many small TFT displays implement a SPI interface.

## I2S
I2S is a serial protocol specifically designed to transfer stereo audio data. The communication involves three lines: a clock line, a word select line and a data line. The word select line determines if the transferred data is for the right or the left audio channel. External DAC modules and I2S microphones can be easily interfaced to the microcontroller. Using these external modules instead of the built in ADC and DACs of the microcontroller usually result in better quality audio interfaces. 

## ADC
Most microcontrollers implement Analogue to Digital Converters on the chip. Normally multiple IO pins of the microcontroller can be connected (via software configuration) to the ADC so that multiple analogue signals can be digitized by the ADC. If precision is required, care has to be taken to keep the noise on the analogue line low. For this reason the analogue circuity of the ADC in the microcontroller often has separate power connections (analogue power and ground) which can be fed from an independent low noise power supply. 

## DAC
Often also digital to analogue converters can be found in  microcontrollers. These can be used to play back audio signals (music or speech) or to implement programmable waveform generators. 

## Timer, Counter, PWM
These units can be used to generate periodic pulses. PWM means pulse width modulation: the PWM allows to generate pulses of programmable width in programmable intervals. For example with these pulses you can for control servo-motors.  The counters can count events occurring on a given IO pin (essentially counting signal transitions on the pin). The unit can generate interrupts to the processor on various programmable occasions. On an interrupt the processor jumps immediately to a previously defined subroutine to handle the event which lead to the interrupt. After this it resumes the operation it was doing before. 
