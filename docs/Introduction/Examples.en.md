# Examples of Microcontrollers

What follows is a short incomplete list with some well known microcontrollers. There are many more on the market. 

## Atmel Controllers ##
Atmel is producing a big variety of different microcontrollers. Some of these have become very famous via the "Arduino" boards which are widely used in hobby projects but also in research and other application areas. ATMEL is producing different families of microcontrollers.

### AVR family of microcontrollers ###
AVR microcontrollers are based on the Havard architecture. The processing unit is a 8 bit RISC (Reduced Instruction Set Computer). They implement an on-chip flash storage to hold the application and possibly some configuration data. The an advantage of the relatively simple architecture is that each instruction uses a clearly defined number of clock cycles to complete. This allows to write very efficient and predictable assembler code for these chips. The AVR controllers are divided further into sub-families. The most used ones are:

*tinyAVR*: The flash size ranges from 512 bytes to 32 kB. The clock frequency from 1.6 to 20MHz and the available RAM from 64 bytes to 3072 bytes. Obviously this family is suitable for projects with limited complexity. 

*megaAVR or ATmega series*: 4-256kB flash, 256 to 16384 bytes of RAM and 1.6 to 20MHz clock frequency. This series is used in several Arduino development boards. 

*XMEGA series*: similar to the ATmega series but with some advanced features DMA and cryptography support. 

### Usage ###
In general ATMEL are once programmed with a "Bootloader" program. This program is loaded once into a flash region (e.g. at the highest address range of the flash) which is then protected to avoid accidental overwriting of the bootloader. The bootloaded is a small application which is automatically run at startup (i.e. at reset or power up). Once started the bootloader waits for a specific time (usually order of seconds) for a specific signal at the interface to the computer. If the signal is detected the bootloaded waits for the computer to send new application code which is then programmed into the flash (this sequence also includes the instructions to erase the flash before it is being programmed.) If the signal is not detected in a given time interval after startup, the bootloader proceeds to start the application loaded previously into the flash (if it exists). 

### Blockdiagram ###
Below a block diagram of the AVR328 microcontroller is shown. It contains many of the items discussed in the previous sections. 

![ATmega328_Block_Diagram](figures/ATMEGA328P-Block-Diagram.jpg)

Some additional explanations: 
   
   - The block NVM programming (Non Volatile Memory programming) contains logic to program the Flash and the EEPROM when no bootloader is present on the system. This logic is needed to program a "blank" device coming from the factory for the first time. (It is also needed if the bootloader for any region becomes corrupted.) 
   - The EEPROM is a small memory region containing some configuration data for the device. It contains data words where single bits (also called "fuses") can be changed to define some functional features of the device. These fuses can only be programmed once (this is why they are called "fuses"). If you make a mistake you have to take a new chip. 
   - USART is an enhanced UART allowing several different operating modes. 
   TW{n} are two wire interfaces like I2C, CANbus...
   - EXTINT are inputs for external interrupts. If a line is activated the Controller interrupts its current activity and immediately jumps to a previously defined "interrupt service routine" containing code which needs to be urgently executed. This is often used when data is arriving over some external interface and has to be read before a small buffer memory overflows. It can also be used to react quickly to some alarm conditions in external hardware.
   - TC blocks are Timer/Counter blocks which can be used to count signals or output different kind of pulses.
   
## Raspberry Pi RP2040 (and the Pico board)
The RP2040 chip is based on a 32 bit dual core Cortex-M0+ microcontroller. This processing unit is developed and licensed by ARM. The processor core is extremely small and targets the production of very cheap microcontrollers.
The two cores run at 133MHz (but can be over-clocked up to 400MHz). Each core provides an integer divider and two interpolators. The integer divider performs 32 bit divisions and provides results after 8 clock cycles. (The integer division and the remainder of the division are provided in thre result registers.)

The chip has 264kB of RAM. The flash is provided externally (up to 16MB suppoerted via a QSPI bus-controller). 

On the RP2040 chip the processing core is interfaced to a large variety of peripherals as it is typical for microcontrollers. (DMA engines, ADCs, Serial interfaces like I2C, SPI, UART, PWM). The microcontroller in addition features a USB interface (can be operated as USB-host or as USB-device), and programmable IO units (see below). Two of the more exotic but powerful features are discussed in the following sections. The following figure shows the block-diagram of the RP2040. It is worth noting that the peripherals can be connected to almost any of the GPIO pins by programming an internal switching matrix. This is very convenient for custom boards and eases the design of the PCB layout. 

![RP2040 Blockdiagram](figures/RP2040_Blockdiagram.png)

Raspberry Pi sells an inexpensive development board called Pico which can be used to implement projects involving the RP2040. Other companies sell similar boards with various different features and sizes. Raspberry Pi also sells a version with a wireless interface added to the board (Pico-W). The board has the same size and pinout as the Pico board and is an interesting solution for projects requiring network connectivity (IoT, remote controls, ...)

This is the Pico-W (version with Wireless chip: the silver (shielded) device contains the RF circuit for the wireless connection).

![Pico-W](figures/RASP_PI_PICO_W_01.jpg)

In the following subsections two particularly interesting features of the Pico
are mentioned. There is no in depth discussion of these topics but interested
readers are encouraged to browse the offical RP2040 documentation to explore
these powerful features.

### The interpolators

The RP2040 implements 2 so called interpolators for each of the two cores. 
The following picture shows a block diagram of an interpolator unit in the RP2040:

![Interpolator_Block_Diagram](figures/RP2040_Interpolator.png)

The interpolators can be used to do operations like linear interpolation, clipping values to a maximum and minimum, or for blending operations. These operations are useful when processing image data or audio data. The interpolators are used in the audio SDK of the RP2040. 

More information about the Interpolator can be found in the RP2040 data sheet. It is a slightly involved topic (I myself did not yet dive into it and cannot further comment.)
 
### RP2040 PIO
In addition to the two CORTEX cores the RP2040 processor contains two PIO blocks which each contain 4 programmable state machines which execute small programs which can be used to implement various serial protocols on the output pins. Additional standard interfaces like UARTS, i2c, spi, i2s (for sound) etc can be programmed and hence the number of these interfaces can be adapted to the need of the project. The operation of these interfaces then do not require CPU resources and hence they perform equivalent to the hardware peripherals already implemented in the microcontroller (but sometimes you meed more interfaces than implemented in hardware on the chip, and then you can just create these additional interfaces with PIOs.). The programmable interfaces also allow you to implement custom interfaces. For example in the world of radio controlled models there is a wide spread protocol which corresponds to a standard UART protocol but with levels inverted. Such a "inverted polarity" UART can be easily implemented with the RP2040 and saves the use of an external inverter or transistor to invert the signals. 

Again, using and programming this PIO is a bit involved and further information can be found in the data sheet for those who are interested. In total there are just 9 different assembler commands for the statemachine. They are sufficient to  implement all kind of serial interfaces. 


## ESP32 and it's variations
The expressif ESP32 microcontroller series is particularly interesting since these chips contain a wireless interface without the need of additional external chips. In the exercises we will work with this processor. 
There are many different variants of this processor. Some have two processor cores and some only one. They run with either 160MHz or 240MHz clock frequency. In addition to the processor cores they contain Ultra Low Power Processors (ULPs) which can execute simple tasks with extremely low power consumption. This is useful for handheld devices where often no processing is required but the device is waiting for some input data. In this case the main processors can be put in a sleep mode to not consume power whereas the ULPs handle the input of the data and wake up the main processor when something has to be done. A functional blockdiagram is shown below:

![ESP32 functional blockdiagram](figures/ESP32-functional-block-diagram.png)

The wireless interface is able to handle WIFI and Bluetooth protocols. Concerning the peripherals it is worth noting that the ESP32 features a DAC (8 bit), and a I2S serial interface (used to transfer high quality stereo audio data via a serial protocol). In addition to the usual peripherals you can find touch sensor interfaces, a CAN interface, an IR interface (to implement Infra Red remotes and an Ethernet MAC). Later models have a native USB interface. Others (like the development board used in this course) need an external chip to convert data traffic from a UART to the USB protocol. The chip is operated at 3.3V. 

There is one disadvantage for some of the ESP32 microcontroller which needs
to be considered in case you need very high performance in some sections of
your application: It is not possible to program the ESP32 controllers which
are based on the Tensilica Xtensa LX6 or LX7 processors in assmebler since
the instruction set for these processors is not public available. However,
there are ESP32 microcontrollers based on a RISC-V architecture and these
can be programmed in assembler. (Examples are ESP32-C3, ESP32-C6,
and some more (see Wikipedia or the Expressif web site). 

## References
[1] [RP2040 data sheet](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)

[2] [Expressif website](https://www.espressif.com/en/products/hardware)
