# Overview of this section

In this section we discuss the following topics:

- What are microcontrollers (opposed to computers or microprocessors) 
- The main characteristics of microcontrollers
    - In particular we discuss some of the most common peripherals of microcontrollers
- Some Examples of popular microcontrollers

