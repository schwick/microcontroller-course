# Topics #
In order for MQTT to be useful members of the MQTT network need to be able to publish information and to receive information in order to do something with this. Since all kind of different information might be published in the same network each piece of information needs an unique identifier. This identifier in MQTT is called the topic name. The information being published can be anything (i.e. any collection of bytes). But often a human readable format is chosen (like json-documents [1]). 

## Topic names ##
Topic names in MQTT are organized hierarchically like directories in a directory tree. The syntax is actually the same as the the file-path syntax in Linux systems with slashes ('/') separating the hierarchical groups. Here are typical topic names for a Home MQTT network as they are often used in "smart homes":

	home/living-room/light
	home/living-room/store
	home/living-room/temperature
	home/bedroom/light
	home/bedroom/temperature
	home/garage/main-gate
	home/kitchen/light
	home/kitchen/temperature
	home/kitchen/fridge/temperature

Spaces are allowed in topic names. Topic names are case sensitive. 

It is completely up to the user how the topic hierarchy is organized. Topics do not need to be created before they are used, i.e. a client can publish any topic it wants. However, the user needs to pay attention here: a typo in the topic name on the publisher or on the receiving side leads to problems without that the system is giving any diagnostics for this. 

How to best structure the topic hierarchy depends on how the data is used by the "consumers" of the data. We will see later how topics can be filtered by the subscribers. This filtering needs to be considered when designing the topic hierarchy.

## Topic subscription: filtering and wildcards ##
When subscribing to a single topic the complete topic is used. However, often it is useful to subscribe to many topics at the same time. For example in a smart home you want to create a display giving an overview of the temperatures in all rooms. If you were to use subscriptions to single topics you would have to create a subscription for every room with a sensor. If later you install additional sensors in other rooms, you would have to change the software to reflect this change and you would have to create additional subscriptions. It would be much nicer to be able to subscribe to all temperature values at once. 

This is possible with the concept of wild-cards. Consider the following expression:

	home/+/temperature
	
The '+' sign is a wild-card for anything between the slashes (i.e. any room in our example). When using this expression in the subscription the client will be updated with all temperature values in the house. Note that this expression will subscribe to home/kitchen/temperature but NOT to home/kitchen/fridge/temperature. 

There is a second form of a wild-card which covers multiple levels of the hierarchy, however, it must be the last character of the expression following directly a slash. 

	home/kitchen/#
	
will subscribe to all topics published under home/kitchen. In our example you would get the topics

	home/kitchen/light
	home/kitchen/temperature
	home/kitchen/fridge/temperature
	
If you subscribe to the topic '#' alone you will be updated with all topics of the broker (which may create substantial system load and might slow down the entire MQTT system in case of large systems).

## Internal MQTT system topics ##
There is one restriction to topic names: they must not start with a '$' sign. This is because the MQTT system uses topic names starting with '$' for internal purposes. Important system information and statistics is published in these topics and they can be useful to monitor a MQTT system. In most brokers these topics start with $SYS. Clients are not allowed to publish to topics starting with '$'. The actual system depend on the broker implementation and are not standardized. The $SYS topics of the mosquitto broker are documented in its man-page [2]

## Practical notes on topics ##

* Avoid to use a leading slash in your topic names as this introduces an additional topic level with an empty name which can cause confusion. 
* Even though it is allowed, do not use spaces in topic names. It can make life miserable in the software of the client due to the different representations of spaces in various character encoding and due to the hassle they cause in the interpreting code (you will find that often you need to do extra programming if you allow spaces in the topic names.)
* Keep topic names reasonably short (especially if you work with small microcontrollers).
* Do not use exotic characters (if you can, only use ASCII characters).
* You can consider (if useful in your case) to integrate an unique client identifier in the topic name so that you know at the receiving end who published the message.
* Be careful when subscribing to all messages using the '#' topic. This may have fatal consequences for you system in case of large message traffic. It might work in the beginning but later when the system grows it could cause problems. If you need to do something with all messages see if the broker allows another way to program a hook directly in the broker (as far as I know this possibility does not exist for the mosquitto broker).

## References ##

[1] [JSON format](https://www.w3schools.com/js/js_json_intro.asp)  
[2] [Mosquitto man-page](https://mosquitto.org/man/mosquitto-8.html)
