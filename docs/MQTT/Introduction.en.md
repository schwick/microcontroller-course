# Introduction to MQTT #

MQTT (MQ Telemetry Transport, where MQ stands for a IBM middleware product line in the '90) was originally developed by IBM engineers to monitor oil pipelines over satellites. It has been designed for communicating with devices of limited resources and this is why it is today very successful in house automation and similar projects (often referred to as IoT). The MQTT needs a lossless underlying protocol which usually is TCP/IP. "Lossless" means that if you send something you know for sure it will arrive at the receiving end since the protocol will resend the data until it correctly arrives (... of course this might take forever if somebody cuts the line...).

The protocol is very easy to use and all complicated networking issues are dealt with under the hood. 

In this course we give an overview on MQTT v3 (there exists an enhanced version MQTTv5 but this is not yet supported by all software packages for microcontrollers).

## Overview ##
MQTT is a publisher subscriber messaging protocol. This is explained in the following. On a MQTT network there is always at least one unit called the "Message Broker" and a number of "Clients". All clients connect to the Message Broker via the underlying network. Data is exchanged between clients by sending around data structures called "topics". A topic has a name and a payload. The payload can be any collection of bytes whereas the topic name looks like a directory-path, i.e. topic names are organizer hierarchically. 

If a client desires to "publish" an new topic (or new data for an existing topic) it simply sends the data with the topic name to the Broker. 

If a client desires to receive data for a given topic it sends a "subscribe" message to the Broker for the topic. It will then receive all updates for this topic whenever a client updates the data for the topic. 

Of course, clients are allowed to publish and to subscribe to topics as can be seen in the illustration below.

These simple concepts have important consequences for MQTT networks: The implementation for clients can be very simple (as intended). No complex addressing schemes are required. The client just need to know how to address the broker. Clients can be inserted into the network at any time. They can publish whatever they want to and they can subscribe to any topic they want. However, the knowledge about existing topics and their names needs to be transferred on a higher level: the applications subscribing to topics need to know about their existence. As we will see it is possible to develop to some level discovery mechanisms for available topics, however some conventions are always involved. 
The simplicity of the clients comes with a price, and this is the need of a central unit, the broker, which is always up and running and stable. Without the broker nothing works in a MQTT network. However, there exist methods to implement redundancy for brokers if need be. 

In the following section we discuss these mechanism in some more detail but the main concept of this simple protocol is already explained with these few paragraphs here. 

![Publisher Subscriber concept](figures/MQTT_Diagram.png)

## References ##
Many books exist about MQTT, however, also the Internet is full of useful resources to learn about MQTT and you will easily find some documentation according to your taste. Therefore here only very few references:

[1] [mqtt.org](https://mqtt.org/)  with a "Getting Started" section.  
[2] <https://www.hivemq.com/mqtt-essentials/> The company HieveMQ has a lot of resources which are of general interest (of course they also promote their products in the articles.)  
[3] [Mosquitto](https://mosquitto.org/) A popular MQTT Broker which runs on small systems (e.g. raspberry pi) is the Mosquitto broker. It is usually available for the major Linux distributions. We will use this in the exercises.  
[4] <http://www.steves-internet-guide.com/mqtt-basics-course/> Short and concise text (...embedded in a lot of advertisement though...)

