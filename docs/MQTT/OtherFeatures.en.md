# Other features #

## Retained message ##
We already discussed the mechanism of retained messages in the section about the publish messages. 

If for any reason you want to delete the retained message stored in the broker there is one way to do so. A client needs to publish a message to the specific topic with zero bytes as payload. In this case the Broker will delete the retained message and will not send anything to newly subscribing clients. 

 