# Publishing and Subscribing #
After having studied the general concepts of message transfers in MQTT we now look in detail at the Publish and Subscribe messages. 

## Publishing topics ##

The following diagram shows the data fields to be filled by the publishing client. 

![](figures/publish_packet.png)

The publishing client sends the message to the broker which then forwards the message to all clients which subscribed to the relevant topic.

The packetId is a number to identify the data transaction (i.e. the publish transaction in this case.) It is only relevant when the QoS is larger than 0. In that case we have seen in the previous section that further packages are exchanged between the client and the broker (a puback message is sent for QoS 1 and for QoS 2, Puback, Pubrel and Pubcomp messages are exchanged.) To unambiguously associate these messages to the transaction (i.e. to the publish message) an unique packetId is create by the initiating client and written into the corresponding data field. (Note that in a given moment in time more than one publish message from a specific client could be in progress on the broker (or arriving at a client), and these packetIds allow to associate all exchanged messages to the correct transaction). The client is not allowed to re-use a packetId until the transaction is entirely completed, of course.

The retainFlag indicates to the Broker it should store the message for the topic until the next messsage for the same topic comes in. Like this the Broker has the most up to date value for the topic in memory. When a client subscribes to this topic it is immediately updated with this value after the subscription. Hence the client immediately has the last value for the topic and does not need to wait until another client publishes a value for the topic. As you can imagine this is a very useful feature for systems in which the subscribing clients need to be informed about the state of a system which rarely changes its state. 

As mentioned before, the payload can be any collection of bytes. A very common data format use is the json data format (essentially key/value pairs or arrays). But strings or numbers are perfectly valid payloads.

Finally the dupFlag is used in QoS 1 and QoS 2 transaction to indicate that a message is being re-sent since the corresponding acknowledge message from the other side has not arrived. It allows to guarantee that a message is only processed once. 

## Subscribing to a topic ##

The following diagram shows the data fields to be filled by the subscribing  client.

![](figures/subscribe_packet.png)

The message has to contain an unique packetId for the same reasons mentioned above. This id is then followed by an arbitrary number of QoS/topic pairs. Therefore the client can subscribe to many topics in one go. 

Subscriptions are acknowledged by the Broker with suback messages:

![](figures/suback_packet.png)

The Suback message contains a return code for each subscription. It either indicates the QoS level for which the subscription is acknowledged or a failure code (0x80) in case of unsuccessful subscription. 

Clients can also unsubscribe from previously subscribed packages:

![](figures/unsubscribe_packet.png)

The Broker acknowledges the unsubcritption with a Unsuback packet containing just the packetId:

![](figures/unsuback_packet.png)

