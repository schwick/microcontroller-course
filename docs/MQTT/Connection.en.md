# Connecting Clients to a Broker #

In this section we discuss how a client connects to a MQTT broker. We do not discuss the lower level protocols (like TCP/IP) which are used "under the hood" but only the mechanism specific to the MQTT protocol. We also do not discuss the API for connecting to the broker (which depends on the programming language and the specific library used.) However, the API of a MQTT library for microcontrollers will be explained/used in the exercises. 


The figure below shows the data fields present in the MQTT connect message sent by the client.

![MQTT Connect message](figures/ConnectionFields.png)


| Field name | meaning|
|------------|--------|
|clientId | A unique name for the client in the MQTT network. It is up to the user to make sure that the clientId is unique. The broker uses the clientId to store details about the session to the client. Only if a connection with a "clean-session" is being established the clientId can be left empty, since in this case the broker does not need to store session information (see below.)|
|cleanSession|When the CleanSession flag is false a "persistent" connection is requested by the client. This means the Broker will save information about the connection to the client so that it can be used in case the client was disconnected for some reason and then re-connects. All topics the client subscribes to, are being saved so that the client does not need to re-subscribed to the same topics again when it re-connects. In case the QoS(Quality of Service, described below) is larger than one, also all messages for the client which have not been acknowledged by the client are being saved, so that if the client re-connects it will miss not a single message to which it had subscribed since they will be all sent after successful re-connection (this might cause some significant network traffic and load on the computers involved...).
|username/password|A simple security mechanism for establishing connections to the Broker which can maintain a list of users. However, this data is transferred in plain text. Therefore it is strongly recommended to use protocol encryption when the Broker is exposed to the the Internet.|
|lastWillMessage| The lastWillMessage is a message which is stored on the Broker for the client and will be sent out to clients (which have subscribed to the lastWillTopic) if the client for any reason disconnects.| 
|lastWillTopic| The topic of the lastWillMessage.|
|lastWillQoS| The QoS (see later) of the lastWillMessage.|
|lastWillRetain|When this flag is set the Broker will store the message and sent it out to clients which subscribe to the lastWill Topic immediately after the subscriptions. Like this the client receives immediately the last updated value for the topic and does not need to wait for the next publish event on that topic. (This flag exists for all messages publishing a topic to the Broker) |
|keepAlive|A time interval indicating the maximal time without message exchange before the Broker closes down the connection. In order to avoid the connection to be closed by the Broker the Client is sending "ping" messages to the Broker in time intervals smaller than this specified time. The Broker has to acknowledge these messages with a ping-response message. Like this both sides can detect a non working connection.|

The usefulness of the keepAlive mechanism needs to be explained: In principle there should be no need for this mechanism on MQTT networks running over TCP/IP. This is because the underlying TCP/IP protocol in theory should be able to detect if a connection is broken and in this case it informs both sides of the connection. However, especially on mobile and wireless networks, situations can arise that TCP/IP connections are "half dead" so that messages are simply suppressed or get lost without that it is noticed by the software. To cope with this situation the keepAlive mechanism is useful since eventually the higher level MQTT protocol itself is able to detect a non-working connection (via the missing ping or ping acknowledge messages).

