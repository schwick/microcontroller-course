# Microcontroller Course

In this course the characteristics of microcontrollers will be presented. This is a practical course where emphasis will be put on practical exercises. However, the course will be accompanied by some lectures which are summarised on these web pages. In addition this web sites for interested students who want to get a deeper knowledge in this topic.

The exercises require some knowledge of the python programming language. 

An outline of the course is given below:

1. The introduction will present the main characteristics of microcontrollers and how they differ from full fledged computers. Examples of various microcontrollers are presented and the choice of the model used for the exercises is motivated. 
1. Typical development environments to program microcontrollers are discussed. In the exercises the installation and the first steps with one of these environments is performed. 
1. Some microcontroller peripherals are discussed in the subsequent chapter. The selection contains those peripherals which are typically present in different kinds of microcontrollers. Simple examples dealing with some of these peripherals are dealt with in the practical exercises.
1. An introduction to the MQTT protocol will give students the necessary knowledge to start projects using this protocol. With the rise of what is called "IoT" (Internet of things) this protocol has become an important player. The exercises will present some projects using MQTT.

In addition some further topics are discussed during the lectures. Among these are
   - A short discussion of memory management in computers
   - Basic Networking
   - An introduction of "asyncio" in python.
   - Optionally: some aspects of the FreeRTOS operating system (more of an Appetizer than an Introduction) 

Some basic understanding of these topics is extremely useful when building new projects even though they are partly "hidden" from the user in high level languages like python. (Less so in C or C++). However, during debugging of programs or designing of projects a basic knowledge is of great use. 
