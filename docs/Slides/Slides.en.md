# Slides of the lectures #

[CourseSlides_1: Characteristics, Memory handling, Development environment](CourseSlides_1_BasicDevEnv.pdf)

[CourseSlides 2: Typical Peripherals in microcontrollers](CourseSlides_2_Peripherals.pdf)

[CourseSlides 3: An OLED display](CourseSlides_3_OLED.pdf)

[CourseSlides 4:Networking "crash course"](CourseSlides_4_Networking.pdf)

[CourseSlides 5:Asyncio in micropython](CourseSlides_5_Asyncio.pdf)

[CourseSlides 6: Introduction to MQTT](CourseSlides_6_MQTT.pdf)

[CourseSlides 7: Going further...](CourseSlides_7_Outlook.pdf) (Contains a list of possible microcontroller projects which you can choose from or which you can use as inspiration for your own project.)
