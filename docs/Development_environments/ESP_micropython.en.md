# Micropython #

[Micropython](https://micropython.org "Micropython link") [1] is 
developed for many different microcontrollers. The current version (October 2024) is based on Python 3.4 (with some updates from later versions). Micropython is very well [documented](https://docs.micropython.org/en/latest/reference/index.html)
including specific features for various board types [2].


## REPL ##
Micropython comes with an interactive interpreter called REPL (Read Evaluate Print Loop). It provides a shell like environment (i.e. like any terminal) in which you can directly type python commands which are then executed. This mode is very handy
to make the first steps or try out simple commands. Details are described on the documentation website 
<https://docs.micropython.org/en/latest/reference/repl.html>

## mpremote ##
To communicate with the host computer (where for example you might want to edit your python programs) micropython provides a command line utility mpremote. This utility is written in python itself and is running on your development PC/laptop. You can install it with "pip". 

mpremote allows you to interact with the micropython system installed on the microcontroller. It can send commands which are then executed on the microcontrollers. For example you can reset the controller. The micropython system installs a file-system on the microcontroller flash which can be used to store python libraries and programs. mpremote allows you to interact with this file-system (i.e. store or delete files from this file-system, create and delete directories, ...). 

A very convenient feature is the possibility to "mount" a directory of the host computer as the file-system of the controller. This means that temporarily the microcontroller "sees" the "mounted" directory as the top of the directory tree of its file-system. You can edit the files in this directory structure with your favourite tools/editors on your PC and at the
same time you can execute the programs which you have written on the microcontroller. This is a very convenient way to develop in python. 

mpremote even contains a pip like utility called "mip" which allows you to install micropython packages from the internet directly on the microcontroller.

## Byte code ##
As the "normal" python system can handle pre-compiled python files (they have the extension .pyc) micropython also can handle pre-compiled binary files which must have the extension ".mpy". These files may contain python byte code i.e. pre-compiled python files) but on some architectures they may also contain native machine code which can be generated with c-compilers for example. 

## Specific micropython features ##

### Interrupt handling and ISRs ###
In micropython it is possible to write interrupt handlers (also called "Interrupt Service Routines" or ISRs) in python (this is 
not possible for "normal" python on a PC). 

However, there are some important restrictions to the code in an interrupt handler. Interrupt handlers are not allowed to allocate any memory. This is because the interrupt may occur at any unforeseeable time and it could occur when the microcontroller is in the middle of a memory allocation activity when executing some code. If it is then interrupted and starts another memory allocation without having finished the previous operation
the memory of the microcontroller ends up in a mess (i.e. the data which keeps track of the memory locations will end up in an inconsistent state) and the program will crash. Hence interrupts handlers are not allowed to create objects or to use floating point operations (since floating point arithmetic in python create float objects. The same is valid for long integers which contain more than 32 bytes: these must not be used in ISRs.). 

Programming ISRs is not easy and requires experience and care. The difficulty here is to correctly estimate the consequences when the main program flow is interrupted at any arbitrary point in time and what consequences this might have on the activities of the ISR (which in some way needs to communicate via data exchange with the main program since otherwise it would be completely useless, of course). Another difficulty of writing ISRs is that in general they need to be "re-entrant" which means that it is possible that the ISR itself is interrupted by an interrupt and the ISR is entered again before the previous function execution has finished. This easily leads to inconsistent or corrupt data within the ISR itself. 

A commonly used technique to fight some of these difficulties is to disable interrupts of the microcontroller in "Critical sections" of the code so that the programmer knows that during that section no interrupts can occur. 

In the micropython documentation you find an entire section about interrupt handlers [7].


## Micropython and memory ##
To run a micropython program the python code first needs to be compiled into byte-code. This bytecode is then executed and by the python run time machine. The compilation happens at the import statement and requires RAM itself. If not enough RAM is available on the microcontroller the program ends with an memory exception. 

If necessary it is possible to "cross compile" the python code into byte compiled code (".mpy" files) with a cross compiler which runs on the host computer. This cross compiler needs to be installed (eg with ```pip install mpy-cross```). The mpy files can then be transferred to the file-system of the microcontroller and do not need to compiled anymore during the import step. Hence this saves some memory for running the program. 

The micropython webpage gives further hints on how to optimize python programs for microcontrollers and how to keep memory consumption in limits [6].

# Micropython on the ESP32 #
The implementation of micropython for the Expressif microcontrollers (including the controller we are using: the ESP32 on the ESP32-WROOM-32E development board) has been produced using the development system delivered by Expressif (called ESP-IDF). This system is build on FreeRTOS. However, the details of FreeRTOS and the the ESP-IDF do not need to be known when using micropython on the ESP32. Since python is a language which probably everybody in this course knows and since we want to learn the features of the Microcontroller hardware within a short time we use this system as the development environment for our 
course. This avoids to go in depth into the C or C++ programming on microcontrollers and studying the complex (but very interesting) features of FreeRTOS. However, it is recommended to all those who want to go further in using microcontrollers to dive into these topics to be able to get the best performance out of their microcontrollers. 


# References
[1] <https://micropython.org>  
[2] <https://docs.micropython.org>  
[3] <https://docs.micropython.org/en/latest/reference/mpremote.html>  
[4] <https://docs.micropython.org/en/latest/reference/mpyfiles.html>  
[5] <https://docs.micropython.org/en/latest/reference/isr_rules.html>  
[6] <https://docs.micropython.org/en/latest/reference/constrained.html>  
[7] <https://docs.micropython.org/en/latest/reference/isr_rules.html>  
