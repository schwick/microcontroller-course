# Networking #

The ESP32 is a particularly interesting microcontroller since it has a wireless network interface integrated. This interface can be configured to connect to a WLAN (this is what we will use in this course) but it also allows you to build an access point. In addition there is a special configuration which can be used to build a network among multiple ESP32 devices. If you do not need high bandwidth you can configure this special mode in a way that the range of the network connection goes far beyond the one of standard wifi devices. This is particularly interesting when building remote controlled devices (e.g. toys like cars, ships, planes or drones).

In case you are new to networking you find some basic explanations on some of the most important concepts of Networking in the section [Other Infomation / Networking ](../../OtherInfo/Networking.en.md).

## Connecting to an access point ##
By far the most common case will be to connect to an existing wifi network. Your controller is probably connected to some sensors and needs to report the values to some server where they are further processed or stored. Another use-case is that you have some "actors" connected to the microcontroller (i.e. some light switches, or dimmers) and you want to remotely act on these. In most cases you want to have data traffic in both directions which is not a problem for normal wifi networks, of course. 

### Steps to be performed in micropython ###
First we need to configure the device as a so called "station interface" (opposed to an "access-point interface"). In your program you create a station interface by calling the following library routine:

```
import network
sta_if = network.WLAN( network.STA_IF)
```
network.STA_IF is just a configuration number (an integer number) which tells the network.WLAN routine how to configure the networking hardware (as station or as access-point).

We now have to connect to the wireless WIFI network. First we need to "activate" (=switch on) the hardware and then we tell the hardware the ssid and the password to use in order to attempt the connection:

```
ssid = "student14"
password = "$unilab1"
sta_if.active( True )  
sta_if.connect( ssid, password )
```
we assume that the variables ssid and password contain the strings for the SSID and the connection password. (The example above is for the network we use in the course.)

The connection itself takes some time. We can check if the connection is succeeded with the command:

```
sta_if.isconnected()
```
which returns True in case of a successful connection and False otherwise. 

Once the connection succeeds the interface automatically tries to obtain an IP address via the DHCP protocol. We can read out the IP address we obtained by calling

```
ifparm = sta_if.ifconfig()
print( IP: %s" % ifparm[0])
```

The ifconfig() routine returns a tuple with 4 IP addresses:
   - the IP address of the network interface
   - the subnet-mask
   - the gateway
   - the DNS



Behind the scenes a lot of complicated processes take place in order to build up the connection: e.g. finding the correct encryption algorithm and setup the encryption, or obtaining the IP address via the DHCP protocol. However, the library hides this complexity from the user and makes networking on the microcontroller a "piece of cake" and you do not need any expert knowledge. (...however, it is always good to know a bit more when it comes to debugging...)


## Exercise ##
First play around with the commands given above and explore a bit further these commands by using the documentation of the micropython library. 
Then, as a simple exercise, write a routine which you can re-use in the following exercises, which connects the microcontroller to a wifi network. You should give the user feedback on success or failure so the user knowns if the connection was successful or not. You can either give the feedback on the console or, even better, you can use the OLED display. 

In the programs we write from now on we will always need the network connection. In case you want to connect to another network (e.g. at home) you will need to enter different connection parameters. This is may be the point where you should also think to build a generic class which allows you to read configuration data from a file so that you do not have to change the code each time you want to connect to a different access point. Write this class in a way that you can use it also for other configuration parameters which you might need in future projects. Hint: look into the json file format which is very convenient here. It allows to define simple dictionaries which are "key-value" pairs. There is a json library in python which allows to convert the json file into a python dictionary so that this class becomes very simple. An example json configuration file for this exercise looks like this:

```
{
    "ssid"            : "student14",
    "password"        : "$unilab1"
}
```
