# Micropython on the ESP32

This Section describes the installation steps for micropython on the ESP32 
(and specifically the ESP32 board used in this course: the ESP32-WROOM-32E).
More details can be found on the micropython documentation page. 

## Installation
The detailed installation procedure is described on the documentation pages
of Micropython [2].

### Virtual python environment
However, before we go ahead with the installation we prepare a clean virtual 
python environment with all tools we need. (A virtual python environment is
a python installation on your computer which is not centrally installed but
it is installed in a directory tree usually under the users home directory. 
You start with a clean python installation (usually a copy of the central
installation on your computer) and then install all packages you need for 
your project in this environment. This is a clean approach and does not 
pollute the global installation on your computers with packages which you
might only need for one project. The virtual environment can be removed
easily by just deleting the directory tree.)

To proceed create a directory where you want to install the "venv":

```
cd
mkdir mc-python
python3 -m venv mc-python
source mc-python/bin/activate
```

The last command "activates" the virtual environment. If you now use 
python, python3, pip or pip3 the versions in your virtual environment 
will be used. Also python now only knows the packages you installed 
into the virtual environment and will never take any modules from the 
global python installation on your computer. You can verify this by 
typing `which python` or `which pip`. Note that the virtual environment
is only activated in the shell you are currently using (if you open a 
new terminal it is NOT automatically activated). 

You have to remember always to activate the virtual environment when 
you log onto your computer (using the last `source` command listed
above). If you want to de-activate the virtual python environment you
just type the command `deactivate`.

*NOTE FOR WINDOWS* users: Some versions of Windows do not allow the execution of arbitrary scripts from the power-shell. In this case you will not be able to run the "activate" script. If you encounter this problem on your Windows machine try to use the following command to change the execution policy on your machine:

```
Set-ExecutionPolicy Unrestricted -Scope Process
```
Also note that on Windows the file extensions of scripts and executable might be different than on Linux.

### Installing micropython on your ESP32 board

First we need to install a (python) tool running on the computer, which is
able to communicate with the ESP32 over the serial port (via USB). 
In your activated virtual python environment install the tool esptool.py. 

```
pip install esptool
```

At this point also install the mpremote utility which we will need later
in the tutorial:

```
pip install mpremote
```

To communicate with the microcontroller the tool will need to know which
device file to use. The communication via the USB interface to the serial 
UART interface of the ESP32 is done using a device file of the operating 
system. In Linux these files are in the directory `/dev`. Look into this 
directory. 
Then connect the ESP board with a USB cable and after some seconds again
list the directory contents. You should now see a file called `/dev/ttyUSB0`
which previously was not present in the directory. The operating system has 
detected the presence of the new device on the USB bus. It recognized the 
chip on the development board which translates the traffic on the USB line 
to a serial line data traffic which is then forwarded to the ESP32. 

*Remark for Linux users:*
Linux usually does not allow all users to use the device files in /dev. If you look at the output of ``ls -l /dev/ttyUSB0`` you will probably see the line   

```
ls -l /dev/ttyUSB0
crw-rw----+ 1 root dialout 188, 0 Nov 28 22:48 /dev/ttyUSB0
```

This line tells you that the owner of the device is root (the administrator of the linux computer) and that also users which are members of the group "dialout" are allowed to access this device. By default you are not member of this group (you can check this by looking at the output of the ``id`` command). To become member of the "dialout" group you can use the command:

```
sudo gpasswd -a {username} dialout
```

After executing this command you will need to log out and log in again to become a member of this group.

In Windows the communication goes via a COM port. Which COM port is used can be seen by inspecting the device manager in Windows. Some Window systems need to install a driver for the the USB to Serial interface chip. For your convenience the driver for the board we use in the course can be downloaded from reference [5].

In the next step we erase the entire flash of the ESP32 in order to have a
clean environment for micropython.

```
esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
```
(The port might also be automatically detected)

Finally we write the micropython system into the flash of the board (i.e we 
"flash" the micropython firmware"). First download the firmware from the 
micropython web page <https://micropython.org/download/esp32-ota/> [3]: Here the link to the latest firmware image at the time of writing (October 2024): 
<https://micropython.org/resources/firmware/ESP32_GENERIC-OTA-20241025-v1.24.0.bin>

```
wget https://micropython.org/resources/firmware/ESP32_GENERIC-OTA-20241025-v1.24.0.bin
```

Then flash the image:

```
esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 ESP32_GENERIC-OTA-20241025-v1.24.0.bin
```

This takes about 25s. Afterwards power cycle the board or press the reset 
button. This will run the micropython environment.

## Testing the installation and communicating with the board

To communicate with the board we need to use a terminal which transfers the 
commands we type into the computer over the USB port to the serial UART 
interface of the ESP32. In linux we can use the minicom program for this. 
Install the program in your distribution. Then run it with the following 
options

```
minicom -D /dev/ttyUSB0
```

To leave the terminal mode use `CTRL-a x`

You should now see the prompt `>>>` on the terminal which is generated by the 
micropython on the microcontroller. We can now start to enter python commands.
If you type simply return, a new line with the `>>>` prompt should appear. If 
this is not the case, probably the settings of the terminal emulation program
(minicom) are not correct: Type `CTRL-z` and then `o` to configure the 
minicom terminal. Go to the "Serial port setup". You should see "115200 8N1" 
for the communication protocol (8 bits per transferred data work (i.e. 
data is transferred byte per byte), No parity bit and 1 Stop bit). This is 
almost always the standards. For our board it is in addition important that 
the Hardware Flow Control is disabled ("No"). (All other options are also 
set to No on this configuration page).
Once you have adjusted everything (I had to change the hardware flow control
option on my machine) quit using the escape character and then save the 
settings as your default ("Save setup as dfl") so that next time you start
the minicom everything is already set up correctly. Now hopefully everything 
will work. 

The ```mpremote``` utility also contains a terminal application which is 
configured to work with the ESP32. You can simply type ```mpremote repl``` 
to launch this terminal and you should be able to see the micropython prompt ```>>>```. with ```CTRL-x``` you can leave the terminal mode.


For a test type the following:

```
import machine
machine.freq()
```

This command should show you the current processor frequency 
(usually 160000000).




# References
[1] <https://docs.micropython.org/en/latest/esp32/quickref.html>  

[2] <https://docs.micropython.org/en/latest/esp32/tutorial/intro.html#esp32-intro>  

[3] <https://micropython.org/download/esp32-ota/>  

[4] Silicon Labs download page for the windows driver of the USB to Serial chip: <https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers?tab=downloads>

[5] [Windows driver copied to this website for convenience:CP210x_Universal_Windows_Driver.zip](resources/CP210x_Universal_Windows_Driver.zip)

[6] [Micropython firmware image copied to this website for convenience: ESP32_GENERIC-OTA-20241025-v1.24.0.bin](resources/ESP32_GENERIC-OTA-20241025-v1.24.0.bin)
