# The OLED display #
For a microcontroller to be useful it has to interact with the user. Usually it needs to provide some information to the user (or it needs to control some devices). There are a lot of cheap small displays on the market which are ideal for use with microcontrollers since they can be controlled via simple serial buses like I2C and SPI. 

In these exercises we will use a simple OLED display. OLEDs (organic LEDs). OLED displays do not need a background light (like TFT screens) and consume less energy than those. The display has a high contrast since black on an OLED display is really black (you probably all know OLED phone displays which look very nice.) They are also very fast which makes them suitable to display films. However, their lifetime is shorter than that of normal LEDs or LCD or TFT screens. In addition it might happen that a picture "burns into the display" if it is permanently displayed so that it does not completely disappear when new contents is displayed on the screen (the same happened to plasma screens when they first came on the market.)

The display we will be using is a simple black and white display with 128x64 pixels. 

## Principle of operation ##
The display comes with peripheral electronics and a controller chip which does the hard work. The controller chip contains some internal memory which holds the display contents (single bits for each pixel are sufficient for our simple black and white display.) The controller reads out the memory and takes care of switching on and off the OLEDs which are arranged in a Matrix of 128 lines with 64 columns. 

From the user perspective the operation of this display is rather simple. In a first step the controller chip has to be configured appropriately. The chip can be used with different OLED matrices and hence its operation needs to be adapted to the connected OLED matrix. Voltages have to be set, and the controller chip has to be told how the columns and rows of the OLED matrix are connected to the controller (the controller might support larger matrices than the one used in a particular module)

Once the configuration has been done, data is transferred into the internal memory of the controller. It is then displayed immediately on the OLED matrix. 

A simple method to work with these displays is to maintain a copy of the memory (called the "framebuffer") in the application and assemble the contents of the display in this copy with the application software. Once all is done the entire framebuffer is written to the controller memory. This avoids the complication of maintaining a complex logic of how to address single pixels in the matrix. It is also rather efficient since the controller chip supports a "burst mode" where byte after byte can be written into successive memory locations of the controller. Writing the entire displays is so fast that it is hardly perceivable by the human eye. 

## The SH1107 controller ##

When looking at the [datasheet](resources/SH1107.pdf) of the SH1107 controller you see that the chip supports OLED displays up to 128x128 pixel resolution. We use a display with 128x64 pixels, hence the controller chip has memory for twice as many pixels. This factor of 2 can be used to create nice smooth scrolling effects on the display: the contents of the internal memory corresponds to 2 times the screen size meaning that we can upload images which are twice as large as the screen. Which section is displayed can then be configured with some control registers. If these are changed continuously a smooth scrolling effect can be created.

OLEDs operate at voltages which are much higher than usually available in microcontroller electronics. This is why the controller contains a DC/DC converter (or the module contains an external DC/DC converter). The correct functioning of this converter needs to be configured according to the needs of the display matrix. 

The chip also contains settings which control the mapping of the internal memory bits to the pixels. With these settings it is possible to mirror or flip  the screen contents as needed by the application. Usually you can also rotate the image so that you can easily switch between "landscape" and "portrait" mode. But our controller chip does not support this.

As common practice the controller chip provides different ways of interfacing to the outside world. It is the external electronics of the display module which defines which way is used. In general I2C and SPI and a parallel interface can be chosen. We will use a display module which uses the I2C interface-mode of the controller chip.

We also provide the data sheet of the OLED display module in reference [2]. This document contains the details of the oled matric and how it is connected to the controller chip. (For the schematic see the reference [3]). It also provides valuable information on how to configure the controller chip for the connected OLED matrix. You can find the configuration sequence in Section 4.4. It will be helpful in the exercise below.


## First steps with the OLED display ##

Here are pictures from the OLED display we will use during the exercise:

<img src="../figures/M5Stack_display.png" style="width:300px;"/>
<img src="../figures/M5Stack_display_back.png" style="width:300px;"/>

Similarly to the sensor the OLED display is controlled via an I2C bus. Connect the display to the microcontroller and verify that you can detect the display with the scan() routine of micropython (as you have done with the sensor in the previous exercise). Note down the I2C address and compare it to the address specified in the data sheet (it should be the same, of course...)


Now comes the hard part. A configuration routine has to be written in order to correctly initialize the display. As mentioned, the controller chip can be used with a variety of different OLED matrices with different characteristics and for this there are a bunch of registers which control the behaviour of the controller. Via the settings you tell the controller chip how the rows and columns of the display are connected to the controller chip. You get the information from the data sheet of the display module (not from the data sheet of the controller chip). Go through the initialisation sequence of the module and try to understand as many commands as possible with help of the datasheet of the controller. 

Fortunately most of the configuration registers have the right default values after power up (Values at power on are labelled POR (Power On Reset)). But some you have to set correctly in order to make the display work. Study the data sheet and try to understand as much as you can. Note down the parts you do not understand and discuss them during the course. 

Then write a routine to initialize the display (again: best would be to write a re-usable class) Once this is done look at the micropython "framebuffer" class and use this class to display something on the screen (e.g. a line, rectangle or some text)

The following section give some more indications.

### Important sections in the data sheet ###

When you browse the data sheet note that the controller chip is cabled up in the display module to use the I2C protocol. The other possible interfaces you do not need to study since they are not implemented in the hardware. 

There are different data transactions defined: 
   - Commands can be sent to the controller to define the behaviour of the chip.
   - Data can be sent to the internal memory. This data is the pixel data which will be shown on the screen. 
   
A protocol has been defined to make clear if data for the memory or commands are being sent to the chip. The protocol defines that first a control byte has to be transferred to the chip, which contains the information of what is transferred next. Details are in section "I2C bus interface". Study this section carefully and try to understand what the software needs to do in order to transfer commands for the initialization (probably you will need to read this section multiple times... also the English is not really Oxford English but a Chinese translation... that's the real life of an electronics engineer :-) .

In the section "Commands" all possible commands are described in detail. You will not need to adjust voltages. They are set correctly at the POR: "Power On Reset". 

On page 41 you find a very useful summary table of all commands. 

### After the initialization ###

Now comes the easy part and the fun part:

Once you think you have understood how to initialize the chip study the documentation of the framebuffer in the micropython library. Use this buffer in order to display something on the screen. 

You will need to add a routine to your class which copies the data from the framebuffer into the display. You will need to write the data page by page (pages are defined in the data sheet)

You will realize that the orientation of the display is "portrait" mode. However, often you will prefer "landscape" format for this little display, so that you can at least display entire words in one line. While many display controller chips have configuration parameters which allow to rotate the pixel map easily, this controller chips does not feature such a  functionality. Hence it is up to the user to rotate the pixel map before copying the framebuffer into the display. 
Try to write such a program (...without making knots in your brain, please...)

### The circuit diagram of the Display ###
In case you are curious we put here the circuit diagram of the display we use in the course. Where circuit diagrams are often very useful to understand some details, in this case the diagram is not so important to understand how to use the display.

![](figures/oled_sch_01.webp)

On the top left you have a voltage regulator producing 3.3V (internally used by the display) out of 5V (provided by the user).
On the bottom left you have a small circuit generating 9V out of 5V. For the operation of OLED displays always higher voltages than typically used in digital electronics are needed.
On the top right you find how the SH1107 controller chip is connected to the outside world. However, the connection to the OLED matrix is not shown. 
On the bottom right there is a just the connector to the outside world. You see the two power lines and the two I2C lines which have pull up resistors to hold the lines at '1' when there is no device on the bus driving them.

# References #
[1] [Data-sheet of the SH1107 driver chip](resources/SH1107.pdf)

[2] [Example of a data sheet for an OLED module (including display and  controller)](resources/4650_C14586.pdf)

[3] [circuit diagram of the OLED display used in the course](figures/oled_sch_01.webp)