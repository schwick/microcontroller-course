# Preparation for the Exercises #

In order not to lose time during the exercises you can prepare your laptop for having all the relevant tools installed before the start of the course. This is also advisable since we need to install some rather large software packages and we do not know if the network performance in the classroom is sufficient to allow for downloading big packages by all students at the same time. 
Hence try to prepare your computer at home with the following steps.

## Linux computer ##
The course has been developed on a Linux computer and in general it is more comfortable to develop on a linux computer. However, if you do not have Linux installed and do not want to make major changes to your computer, Windows is also perfectly fine for this course. 

In case you have a reasonably modern Lnux installation on your computer you probably do not need to install any extra "tools". You should have a python installation on your system. Check the python version you have on your computer. For this open a terminal (also called console) and type

   `python3 -V`

The output will give you the python version installed. (I would guess that every python3 version will work but of course I have not tried them all...) Modern Linux versions have Python 3.10 or larger installed.

Another tool we need is a "terminal emulator". In the old times terminals were the workstations on computers with screen and keyboard. The keystrokes (and what is displayed on the screen) was transferred to the computer of a Serial Line with a standard protocol called RS232. (You probably heard about Serial Line protocols and UARTs in previous lectures and you will hear more about it in this course.) Modern computers do not have serial lines interfaces anymore but they use the much more powerful USB interface. However, since there are still many devices in use which have a serial line interface the industry has developed chips which convert USB traffic to serial line traffic. Such a chip is on the development board we will use in the exercises. On the computer we now need a program which talks to the USB port emulating the data traffic of the old serial line interface. And this program is called a terminal emulator. 

There are different terminal emulators available in Linux. We will used one which is called "minicom". Hence, use the package manager of your Linux distribution to insall the minicom application.

This is all which is needed from the general Linux side. Now you can download the development system which is a bunch of tools and libraries in python. 

The steps are in detail described in the Section "[Installation of micropython](../Installation/Installation.en.md)" but since you probably do not have a microcontroller in hand you cannot install it yet. You should prepare this installation by 

   1. Prepare the "virtual python environment" as described in the section.
   1. Install the necessary tools in your virtual python environment.  This boils down to run the two "pip install ..." commands described in the section.
   2. Download the micropython environment on your computer so that you do not need to download it from the classroom. This means run the "wget ..." command described in the section (wget downloads files from the web without the need to use a web browser. If you do not have wget (e.e if you use Windows) or cannot install it you can also use a web browser to download the file. The URL is given in the commandline.)
   
Once you have done this your are ready to start the exercises.


## Windows computer ##

It is assumed that you have a Windows 10 or 11 computer (even though also older versions of windows should work.)

In principle Windows 10 and 11 feature a system called "Windows Linux subsystem" which runs a Linux virtual machine on your computer and allows you to work exactly like on a Linux computer using exactly the same tools. Unfortunately in Windows 10 this is not usable for our project since we need access to external hardware via a serial line over USB. And this seems to be very complicated to use in Windows 10 (in Windows 11 it should be straight forward but I had no possibility to try it.)

Therefore we will not use this system in the course. (But if you have Windows 11 I encourage you to try it since then you can easily use all tools and development systems as they are provided for the microcontroller. In particular if you want to go ahead after the course and want to use the development system for writing C and C++ programs I advice you to use either Linux or this virtual linux system on Windows 11.)

Since we do not know if everybody has Windows 11 we decide to use the native windows environment for this course. To check if you have python installed type 

    `python -V`
 
on a console (Powershell). It should show you the python version if python is installed. If you do not have python installed you can get it from the official python ports:

[https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/)

and install it from there.

For a terminal emulator (i.e. a program which allows you to communicate with the microcontroller using using the the keyboard) you can install "[putty](https://www.putty.org/)".

The remaining steps are the same as in the previous section on linux. Since you probably do not have wget on your windows computer use the web browser to download the the micropython environment. The "pip install" commands should work as in linux. (Pip is a python tool to install additional python packages.)

## In case of questions ... ##

In case you get stuck during the preparation or if you have questions concerning the preparation I can try to help you remotely before the course starts. You can drop me an e-mail. The address (I "encode" the address to avoid spam...) is my name (Christoph dot Schwick at cern dot ch)

