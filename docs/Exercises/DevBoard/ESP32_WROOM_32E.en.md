# The development board #

In this course we will use the ESP32 microcontroller from Expressif.
As described before the particularly interesting features of this chip
are the integrated WIFI and Bluetooth interfaces (and it's modest price).

We use a development board with the Microcontroller on it. Development 
boards are produced by chip producers to show off the features of the 
chip they want to sell. The chip is connected to some essential external 
components and the pins of the chip are accessible via pin rows with a 
standard pitch of 2.54mm which allows the use of the development board
on multi purpose bread-boards as we use them during this course. The 
board also provides USB to Serial Communication conversion which is 
convenient since modern PCs/Laptops do not have serial ports anymore.

The ESP32-WROOM module is the silver module on the development board.
You see the PCB antenna sticking out at the side of the module. The
WROOM module itself contains the Expressif ESP32 processor and some
other chips like for example the Flash memory which will hold the 
programs. 

In addition the microcontroller takes its power from the USB interface
so that no external powering is needed. USB provides 5V power, however,
the ESP32 requires 3.3V as supply voltage. Hence the development board
contains the necessary voltage regulator. Since the processor is usually
operated with some external electronic components (sensors, actuators, 
displays, ...) which also need to be powered the development board 
provides the 5V and 3.3V on some of the board pins so that the voltages
can be used to power external components. (If the board is not operated 
with the power coming via the USB cable, these power pins can also be used
to provide the corresponding voltage (either 5V or 3.3V BUT NOT BOTH 
AT THE SAME TIME!!!) to the microcontroller. Of course, if 3.3V is provided
there is no 5V coming out of the 5V pin since there is no power converter
on the board which can generate 5V out of 3.3V). Care has to be taken when
operating components together since some components need 5V powering 
whereas others need 3.3V. The ESP32 is a 3.3V component, which means 
that also the Input and Output pins (IO-pins) are designed for 3.3V
logic levels. Applying 5V logic levels to it's input pins might destroy
the controller or compromise its lifetime severely. In case 5V and 
3.3V components have to be used, so called level shifters have to be
used which convert the different voltage level standards from 5V 
to 3.3V and vice versa. These level shifters exist in the form of 
small "break-out boards" which also can be used on the bread board. 
We will not need these in our exercises.

Here a picture of the board:

![ESP32-WROOM-32E_devBoard_1C](figures/esp32-devkitc-functional-overview.jpg "A photo of the development board")


A very important document is the pinout of the microcontroller board. 
It is shown here:

![ESP32-WROOM-32E-devBoard1C-pinout](figures/esp32-devkitC-v4-pinout.png "Pinout of the ESP32 development board.")

Please note and remember the following: Not all of the GPIO pins can be used.
There are six pins close to the USB connector which are marked with a red
exclamation mark. These pins are internally connected to the Flash memory
chip (inside the WROOM module, i.e. inside the small silver metal box on the 
development board). Therefore these pins should not be used. They are only 
connected to external pins for some expert usage in very special cases. 

In addition you should remember that the pins 34...39 can only be used as 
input pins. In addition they do not have internal pull-up resistors. (A
pull-up resisitor is a resistor with a pretty high value connected to the 
supply voltage. Such a resistor makes sure that a line is on logic '1' if 
nothing else is connected. If another device is connected to such a line and
drives the line low the line will result in logic '0' since the resistor 
value is high and the entire supply voltage will drop over the pull-up 
resistor.)

A very good document is the quickstart guide for the EPS32 of the 
micropython pages [3].

# References
[1] [The development board](figures/esp32-devkitc-functional-overview.jpg)  
[2] [Pinout Diagram](figures/esp32-devkitC-v4-pinout.png)  
[3] <https://docs.micropython.org/en/latest/esp32/quickref.html>
