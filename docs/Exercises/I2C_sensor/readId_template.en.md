``` py title="readId.py"
from machine import I2C, Pin

# Address of the I2C chip and address of an internal register containing an
# identification number. Both values can be found in the data sheet.
# The I2C chip address can also be found with an I2C scan.

BME_ADR = 0x77         # The address of the I2C chip (119 in decimal or 0x77 in hex)
BME_CHIP_ID_ADR = 0xD0 # The register which contains the chipID in the BME_280.
                       

# Stay away from pins 34 ... 39 (only input) and the 6 pins connected
# to the internal Flash of the WROOM module. (See the pin diagram)


# "Pin" and I2C are classes in micropython. We build some objects from these
# classes in order to do something with our I2C device. 

# First we need to create a i2c object: This can be used to create i2c
# commands on the i2c bus. We have to define on which pins we want to
# have which signal of the i2c bus. We also define the frequenecy we
# want to use on the bus.
scl = Pin( 32 )
sda = Pin( 33 )

# Here fill in the constructor to create the I2C object
i2c = ???


# Now we can create traffic on the i2c bus. The first thing we do is
# a "scan" to find out what devices are connected to the bus (obviously
# it should be just our sensor). The scan is a procedure executed by
# the micropython I2C object. For each possible I2C address (128 addresses)
# it tries do access the device (I assume a read access) and watches out for
# the ACK bits which is activated if a slave decodes it's address. If nobody
# replies with an active ACK bit, there is not device on the bus with the
# current address. 
i2cDevices = i2c.scan()

print( "List of I2C devices found during scan:" )

for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()

# Now read the chip Id
# Many I2C devices have internal registers with addresses. The way
# of addressing these registers is the same for all devices: First
# the internal address of the register has to be transferred to the
# device and then the device can be read and the received data will
# be the contents of the register. Since this operation is so common
# it has been implemented in a single function in micropython called
# readfrom_mem (also writeto_mem exists of course). Look up the
# function in the documentation and complete the line below.
chipId = i2c.readfrom_mem ( ??? )

# we print out the address in hex
print("The chip ID is 0x%02x" % int(chipId[0]) )
print()


```
