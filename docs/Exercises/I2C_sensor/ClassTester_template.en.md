``` py title="A class tester for the BME280 class"
from machine import I2C, Pin
from time import sleep
from BME280 import BME280

# Stay away from pins 34 ... 39 (only input) and the 6 pins connected
# to the internal Flash of the WROOM module.
# Suggestion: use pin 32 for scl and pin 33 for sda

??? put here the code to make an i2c object ???


# The following lines are not necessary for the exercise but they
# demonstrate that the i2c interface is working, which is nice to know...
i2cDevices = i2c.scan()

print( "List of I2C devices found during scan:" )

for device in i2cDevices:
    print( "Found device 0x%02x   (dec: %d)" % (device,device) )
    pass
print()


#######################################################################
# All the hard work is done in the BME280 obejct. Sensor specific commands
# and procedures are hidden from the user and the main program becomes
# simple and readable. (Compare this code to the non-object oriented
# version which incluse the entire program in one long program without
# using the concept of "encapsulation" concept of object oriented
# programming languages.) 
#
# Here you should write code which creates an object of our BME280 class
# and then use this object to do every 2 seconds a measurement and
# print out the result on the console.

???


```
