# Publish sensor measurements on MQTT #
Now we extend the previous exercise with some networking. We publish the sensor measurements on a MQTT network. The MQTT broker runs on a dedicated laptop. In case you want to play with this system outside of the course hours you should install the Mosquitto Broker on your own laptop and configure it as your browser (There are binaries ready for installation for all common platforms [1]). To not cause confusion remember to stop the broker during the exercises in the lab, so that you are sure to connect to the central course broker and not to your private one by mistake. 

## The umqtt.simple library ##
To play with MQTT we use a very simple python library called umqtt.simple which is included in the installed micropython version [2]. The documentation of this library can be found in reference [3]. 

Try to understand the short documentation and discuss what the program needs to do to publish some values.

## Extending the sensor program ##
Write a subroutine to connect to the MQTT Broker. To be a bit flexible think of not hardcoding all networking related parameters but put them in a configuration file (for example in json format, which is extremely comfortable to handle in python). Always check that the connection was successful and give feedback via the OLED display.

Then your program should publish the measured values to the Broker. But the frequency of the publications should be configurable (in general it is not necessary to publish values every second. Try to configure the program to publish every 30 seconds for example.)

The topic name should include your name so that the measurements arriving at the Broker are distinguishable (make sure that not 2 students use the same name...).

To test your setup you can install mosquitto on your computer and use the mosquitto_sub utility on your computer to subscribe to the topics your are publishing with your microcontroller. You should also be able to see the publications of your co-students if you choose an adequate topic name with wildcards during the subscription.


## References ##

[1] [Mosquitto Project](https://mosquitto.org)

[2] [umqtt.simple](https://github.com/micropython/micropython-lib/tree/master/micropython/umqtt.simple)

[3] [umqtt.simple API documentation](https://mpython.readthedocs.io/en/v2.2.1/library/mPython/umqtt.simple.html)