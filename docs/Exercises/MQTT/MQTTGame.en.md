# An MQTT (re-) action game #

At the end of this course we want to have some fun. We want to build a game where players are using microcontrollers which are connected via an MQTT network.

## The game ##
The game is rather simple: We measure the reaction time of the participating players and establish a ranking.

### Details of the game ###
Some more details of how the game should look like:

   - Players sit in front of their controller with the OLED screen attached. The screen is dark. At some random time (say 1 to 5 seconds) the screen turns white. At this moment the player has to press a button as fast as possible.
   - Once the player has pressed the button the reaction time should be displayed in milliseconds.
   - After some seconds (e.g. 5sec) the ranking of all participating players should be displayed for another 5 seconds. Since there is only place for 6 lines on the display, only the first 6 players will be displayed (name with reaction time: resolution 10ms)
	- Finally the screen goes dark again and the next round is launched.
	
### Technical challenges ###

   - It should be possible for every player to join or leave the game at any time. (Also with brute force methods like powering down the microcontroller all of a sudden.)
   - The time between the screen going dark to the screen going white (let's call this "waiting time" from now on) should be distributed equally between 1 and 5 seconds so that players are not able to predict the time when they have to react. 
   - It is desirable to have the same waiting time for all participating players in every round. 
   - For this game you also have to use the "subscribe" call of the MQTT lib. Be sure how the mechanism of subscribing to topics works in this simple mqtt library. You will have to define a callback function but note that this callback function is not automatically called when the mqtt message comes to the controller over the network: You will have to constantly "poll" the library to check if messages arrived ! (In larger libraries (not tailored for microcontrollers) this is not the case.)
   - We do not have buttons. But the ESP32 implements capacitive touch buttons on many of it's GPIOs (general purpose pins). You find the description in the ESP documentation. They are easy to use. You have to define a "threshold". For me 	a threshold of "450" works fine so you can take this threshold as a starting point.

## General remarks ##
We first have to discuss and agree on how to implement this game, since all microcontrollers have to work together. It is not important that the code looks exactly the same for all implementations. However the messages which are exchanged with the broker and at which point in time have to be clearly defined so that the various implementations will work smoothly together. This discussion is actually the hard part of this exercise. 

 